<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

use App\Console\InstallCommand;
use Knp\Provider\ConsoleServiceProvider;

$app->register(new ConsoleServiceProvider(), [
  'console.name'              => 'RVDP',
  'console.version'           => '1.0.0',
  'console.project_directory' => __DIR__ . '/../src/',
]);

/**
 * @var $application Knp\Console\Application
 */
$application = $app['console'];
$application->add(new InstallCommand());
$application->run();
