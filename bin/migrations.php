<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Kurl\Silex\Provider\DoctrineMigrationsProvider;
use Silex\Provider\DoctrineServiceProvider;
use Symfony\Component\Yaml\Yaml;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

$dbConfig = Yaml::parse(file_get_contents(__DIR__ . '/../src/config/db.yml'));
$app->register(new DoctrineServiceProvider(), $dbConfig);

$console = new \Symfony\Component\Console\Application();

$app->register(new DoctrineMigrationsProvider($console), [
  'migrations.directory' => __DIR__ . '/../src/migrations',
  'migrations.namespace' => 'Acme\Migrations',
]);

$app->boot();
$console->run();
