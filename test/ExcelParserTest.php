<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\test;

use App\Helpers\Excel\Cache\FileCache;
use App\Helpers\Excel\Parser\ExcelParserImpl;
use DateTime;
use PHPExcel_CachedObjectStorageFactory;
use PHPExcel_Settings;
use PHPUnit\Framework\TestCase;

if (!defined('TEST_TEMP_DIR')) {
  define('TEST_TEMP_DIR', __DIR__ . '/data/tmp/');
}
if (!defined('EXCEL_TEMP_DIR')) {
  define('EXCEL_TEMP_DIR', TEST_TEMP_DIR . 'excel/');
}

class ExcelParserTest extends TestCase {

  public static function setUpBeforeClass() {
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    $cacheSettings = ['memoryCacheSize' => '256MB'];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    parent::setUpBeforeClass();
  }

  public function testCache() {
    $id = sha1("");
    $cache = new FileCache($id, TEST_TEMP_DIR);
    $cache->writeCache([
      'dataKey1' => 'dataValue1',
      'dataKey2' => 'dataValue2',
    ]);

    self::assertEquals($cache->loadCache(), [
      'dataKey1' => 'dataValue1',
      'dataKey2' => 'dataValue2',
    ]);

    $file = TEST_TEMP_DIR . $id . '.cache.dat';

    self::assertFileExists($file);
    $cache->cleanCache();
    self::assertFileNotExists($file);
  }

  public function testValidator() {
    $parser = new ExcelParserImpl(__DIR__ . '/data/notValid.csv');
    self::assertFalse($parser->isValid());
    $parser->purge();
    $parser = new ExcelParserImpl(__DIR__ . '/data/mexx.csv');
    self::assertTrue($parser->isValid());
    $parser->purge();
  }

  public function testFormatter() {
    $parser = new ExcelParserImpl(__DIR__ . '/data/notFormatted.xlsx');
    self::assertFalse($parser->isValid());
    $parser = new ExcelParserImpl(__DIR__ . '/data/notFormatted.xlsx');
    $parser->format();
    self::assertTrue($parser->isValid());
  }

  public function testParser() {
    $session = sha1(date_timestamp_get(new DateTime()));

    $parser = new ExcelParserImpl(__DIR__ . '/data/mexx.xlsx', $session);

    self::assertFileNotExists($parser->path());
    self::assertFileNotExists($parser->metaPath());

    $parser->format();

    $parser->copyFile();
    self::assertFileExists($parser->path());

    self::assertTrue($parser->isValid());

    self::assertEmpty($parser->errors());

    self::assertEquals($parser->getTurnSize(), 6);

    $turn = [2, 3141, 6184, 9187, 12210, 12475];
    self::assertEquals($parser->meta(), [
      'turn'         => $turn,
      'turnPosition' => 0,
      'pointId'      => 0,
    ]);

    $parser->runNext();
    self::assertEquals($parser->meta(), [
      'turn'         => $turn,
      'turnPosition' => 1,
      'pointId'      => 0,
    ]);

    self::assertFileExists($parser->metaPath());

    $parser->runNext();
    self::assertEquals($parser->meta(), [
      'turn'         => $turn,
      'turnPosition' => 2,
      'pointId'      => 0,
    ]);

    $parser->runNext();
    self::assertEquals($parser->meta(), [
      'turn'         => $turn,
      'turnPosition' => 3,
      'pointId'      => 0,
    ]);

    $parser->purge();

    self::assertFileNotExists($parser->path());
    self::assertFileNotExists($parser->metaPath());
  }

  public function testSplitByDate() {
    $session = sha1(date_timestamp_get(new DateTime()));

    $parser = new ExcelParserImpl(__DIR__ . '/data/mexx.csv', $session);

    $splitByDate = $parser->splitByDate();
    self::assertEquals($parser->getTurnSize(), 6);

    self::assertEquals($splitByDate, [2, 3141, 6184, 9187, 12210, 12475]);

    $parser->purge();
  }

  public function tearDown() {
    self::deleteDirectory(TEST_TEMP_DIR);
    parent::tearDown();
  }

  private static function deleteDirectory(string $dir): bool {
    if (!file_exists($dir)) {
      return true;
    }

    if (!is_dir($dir)) {
      return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
        continue;
      }

      if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
        return false;
      }

    }

    return rmdir($dir);
  }

}
