<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\test;

use App\Helpers\DateHelper;
use DateTime;
use PHPUnit\Framework\TestCase;

class DateHelperTest extends TestCase {

  public function testFormatWithDateTime() {
    $dateTime = new DateTime('08-07-2017 18:41:39');

    self::assertEquals(DateHelper::formatWithDateTime($dateTime), '08-07-2017 18:41:39');
    self::assertEquals(DateHelper::formatWithDate($dateTime), '08-07-2017');
    self::assertEquals(DateHelper::formatWithTime($dateTime), '18:41:39');
  }

  public function testWithoutTime() {
    $dateTime = new DateTime('08-07-2017 18:41:39');

    $withoutTime = DateHelper::dateWithoutTime($dateTime);

    self::assertEquals(DateHelper::formatWithDate($withoutTime), '08-07-2017');
  }

  public function testWithoutDate() {
    $dateTime = new DateTime('08-07-2017 18:41:39');

    $withoutTime = DateHelper::timeWithoutDate($dateTime);

    self::assertEquals(DateHelper::formatWithTime($withoutTime), '18:41:39');
  }

  public function testDatePlusTime() {
    $date = new DateTime('08-07-2017');
    $time = new DateTime('18:41:39');

    $dateTime = DateHelper::datePlusTime($date, $time);

    self::assertEquals(DateHelper::formatWithDateTime($dateTime), '08-07-2017 18:41:39');
  }

}
