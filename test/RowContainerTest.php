<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\test;

use App\Helpers\RowContainer;
use App\Helpers\SalesHelper;
use DateTime;
use PHPUnit\Framework\TestCase;

class RowContainerTest extends TestCase {

  public function testFullCheck() {
    $parsed = [
      0 => [
        0  => new DateTime('13-04-2017'),
        1  => new DateTime('17:29:00'),
        2  => '1',
        3  => '1',
        4  => 'Продажа',
        5  => '770',
        6  => '1',
        7  => '23',
        8  => 'прокат',
        9  => '530',
        10 => '1',
        11 => '530',
      ],
      1 => [
        0  => new DateTime('13-04-2017'),
        1  => new DateTime('17:29:00'),
        2  => '1',
        3  => '1',
        4  => 'Продажа',
        5  => '770',
        6  => '2',
        7  => '63',
        8  => 'игрушка',
        9  => '240',
        10 => '1',
        11 => '240',
      ],
    ];

    /**
     * @var $rows RowContainer[]
     */
    $rows = [];
    foreach ($parsed as $row) {
      $rows[] = RowContainer::fromObject($row);
    }

    self::assertEquals(count($rows), 2);

    $sales = SalesHelper::rowsToSales($rows);

    self::assertEquals(count($sales), 1);
  }

  public function testFullSomeItemsCheck() {
    $parsed = [
      0 => [
        0  => new DateTime('13-04-2017'),
        1  => new DateTime('17:29:00'),
        2  => '1',
        3  => '1',
        4  => 'Продажа',
        5  => '770',
        6  => '1',
        7  => '23',
        8  => 'прокат',
        9  => '530',
        10 => '1',
        11 => '530',
      ],
      1 => [
        0  => new DateTime('13-04-2017'),
        1  => new DateTime('17:29:00'),
        2  => '1',
        3  => '1',
        4  => 'Продажа',
        5  => '770',
        6  => '2',
        7  => '63',
        8  => 'игрушка',
        9  => '240',
        10 => '1',
        11 => '240',
      ],
      2 => [
        0  => new DateTime('13-04-2017'),
        1  => new DateTime('17:34:00'),
        2  => '1',
        3  => '1',
        4  => 'Продажа',
        5  => '710',
        6  => '2',
        7  => '63',
        8  => 'игрушка',
        9  => '710',
        10 => '1',
        11 => '710',
      ],
    ];

    /**
     * @var $rows RowContainer[]
     */
    $rows = [];
    foreach ($parsed as $row) {
      $rows[] = RowContainer::fromObject($row);
    }

    self::assertEquals(count($rows), 3);

    $sales = SalesHelper::rowsToSales($rows);

    self::assertEquals(count($sales), 2);
  }

  public function testShortCheck() {
    $parsed = [
      0 => [
        0 => new DateTime('13-04-2017'),
        1 => new DateTime('17:29:00'),
        2 => '1',
        3 => '1',
        4 => 'Продажа',
        5 => '530',
      ],
      1 => [
        0 => new DateTime('13-04-2017'),
        1 => new DateTime('17:29:50'),
        2 => '2',
        3 => '1',
        4 => 'Продажа',
        5 => '770',
      ],
    ];

    /**
     * @var $rows RowContainer[]
     */
    $rows = [];
    foreach ($parsed as $row) {
      $rows[] = RowContainer::fromObject($row);
    }

    self::assertEquals(count($rows), 2);

    $sales = SalesHelper::rowsToSales($rows);

    self::assertEquals(count($sales), 2);
  }

}
