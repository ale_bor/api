<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\test;

use App\Helpers\ValidationHelper;
use PHPUnit\Framework\TestCase;

class ValidationHelperTest extends TestCase {

  public function testNumber() {
    self::assertTrue(ValidationHelper::isNumber("5"));
    self::assertTrue(ValidationHelper::isNumber("5 "));
    self::assertTrue(ValidationHelper::isNumber("5.3"));
    self::assertTrue(ValidationHelper::isNumber("5.3 "));
    self::assertTrue(ValidationHelper::isNumber("-5.3"));
    self::assertTrue(ValidationHelper::isNumber("-5.3 "));
    self::assertTrue(ValidationHelper::isNumber("+5.3"));
    self::assertTrue(ValidationHelper::isNumber("+5.3 "));
    self::assertTrue(ValidationHelper::isNumber(".3."));
    self::assertTrue(ValidationHelper::isNumber("-3bla"));
    self::assertFalse(ValidationHelper::isNumber(""));
    self::assertFalse(ValidationHelper::isNumber("."));
    self::assertFalse(ValidationHelper::isNumber("5.3."));
    self::assertFalse(ValidationHelper::isNumber("-"));
    self::assertFalse(ValidationHelper::isNumber("-."));
    self::assertFalse(ValidationHelper::isNumber("-5.3."));
    self::assertFalse(ValidationHelper::isNumber("-.3."));
  }

  public function testTime() {
    self::assertTrue(ValidationHelper::isTime("0:0"));
    self::assertTrue(ValidationHelper::isTime("0:0:0"));
    self::assertTrue(ValidationHelper::isTime("23:50:59"));
    self::assertFalse(ValidationHelper::isTime("+0:0:0"));
    self::assertFalse(ValidationHelper::isTime("bla"));
    self::assertFalse(ValidationHelper::isTime("25:33"));
    self::assertFalse(ValidationHelper::isTime("23:60"));
    self::assertFalse(ValidationHelper::isTime("23:50:60"));
  }

}
