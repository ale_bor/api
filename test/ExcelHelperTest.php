<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\test;

use App\Helpers\DateHelper;
use App\Helpers\Excel\Parser\ExcelParserImpl;
use App\Helpers\RowContainer;
use App\Helpers\SalesHelper;
use DateTime;
use PHPExcel_CachedObjectStorageFactory;
use PHPExcel_Settings;
use PHPUnit\Framework\TestCase;

if (!defined('TEST_TEMP_DIR')) {
  define('TEST_TEMP_DIR', __DIR__ . '/data/tmp/');
}
if (!defined('EXCEL_TEMP_DIR')) {
  define('EXCEL_TEMP_DIR', TEST_TEMP_DIR . 'excel/');
}

class ExcelHelperTest extends TestCase {

  public static function setUpBeforeClass() {
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    $cacheSettings = ['memoryCacheSize' => '256MB'];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    parent::setUpBeforeClass();
  }

  public function testXLSXParser() {
    $parsed = $this->parse(__DIR__ . '/data/upload.xlsx');

    $this->processParsed($parsed, 135, 135, 135, new DateTime('20.04.2017'));
  }

  public function testCSVParser() {
    $parsed = $this->parse(__DIR__ . '/data/upload.csv');

    $this->processParsed($parsed, 135, 135, 135, new DateTime('20.04.2017'));
  }

  public function testFankytown() {
    $parsed = $this->parse(__DIR__ . '/data/fankytown.xlsx');

    $this->processParsed($parsed, 9, 9, 9, new DateTime());
  }

  public function testEmptyXLSX() {
    $parsed = $this->parse(__DIR__ . '/data/empty.xlsx');

    $this->processParsed($parsed, 0, 0, 0, new DateTime('20.04.2017'));
  }

  public function testEmptyCSV() {
    $parsed = $this->parse(__DIR__ . '/data/empty.csv');

    $this->processParsed($parsed, 0, 0, 0, new DateTime('20.04.2017'));
  }

  public function testMexxXLSX() {
    $parsed = $this->parse(__DIR__ . '/data/mexx.xlsx');

    $this->processParsed($parsed, 12472, 12472, 5487, new DateTime());
  }

  public function testMexxCSV() {
    $parsed = $this->parse(__DIR__ . '/data/mexx.csv');

    $this->processParsed($parsed, 12472, 12472, 5487, new DateTime());
  }

  public function tearDown() {
    self::deleteDirectory(TEST_TEMP_DIR);
    parent::tearDown();
  }

  private function parse($path): array {
    $parser = new ExcelParserImpl($path);
    $parsed = [];
    if (!$parser->isEmpty()) {
      $parser->format();
      $parser->parse(ExcelParserImpl::HEADER_OFFSET, $parser->getRowsCount());
      $parsed = $parser->getData();
    }
    $parser->purge();

    return $parsed;
  }

  private function processParsed(array $parsed, int $parsedCount, int $rowsCount, int $salesCount, DateTime $filterDate) {
    self::assertEquals(count($parsed), $parsedCount);

    /**
     * @var $rows RowContainer[]
     */
    $rows = [];
    foreach ($parsed as $row) {
      $rows[] = RowContainer::fromObject($row);
    }

    self::assertEquals(count($rows), $rowsCount);

    self::assertTrue(SalesHelper::validateRows($rows));

    $filteredRows = SalesHelper::filterRowsByDate($rows, $filterDate);

    self::assertEquals($rows, $filteredRows);

    $errorDates = [];

    foreach ($rows as $row) {
      if (array_search($row, $filteredRows) !== false) {
        continue;
      }
      /**
       * @var $row RowContainer
       */
      $timestamp = DateHelper::dateWithoutTime($row->getDate())->getTimestamp();
      $errorText = "День ещё не наступил";
      foreach ($errorDates as $errorDate) {
        if ($errorDate['date'] == $timestamp && $errorDate['error'] == $errorText) {
          continue 2;
        }
      }
      $errorDates[] = ['date' => $timestamp, 'error' => $errorText];
    }

    $sales = SalesHelper::rowsToSales($filteredRows);

    self::assertEquals(count($sales), $salesCount);

    self::assertEmpty($errorDates);
  }

  private static function deleteDirectory(string $dir): bool {
    if (!file_exists($dir)) {
      return true;
    }

    if (!is_dir($dir)) {
      return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
        continue;
      }

      if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
        return false;
      }

    }

    return rmdir($dir);
  }

}
