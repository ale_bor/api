<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added day closed/open status
 */
class Version20170212103043 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $daysTable = $schema->getTable('days');
    $daysTable->addColumn('closed', Type::BOOLEAN)->setDefault(false)->setNotnull(true);
    $daysTable->addIndex(['closed'], 'idx_sales_closed');
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $daysTable = $schema->getTable('days');
    $daysTable->dropIndex('idx_sales_closed');
    $daysTable->dropColumn('closed');
  }
}
