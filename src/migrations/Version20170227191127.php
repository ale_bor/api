<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Changed sale items price & discount types
 */
class Version20170227191127 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $usersPointsTable = $schema->getTable('sales_items');

    $floatType = Type::getType(Type::FLOAT);

    $usersPointsTable->getColumn('price')->setType($floatType);
    $usersPointsTable->getColumn('discount')->setType($floatType);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $usersPointsTable = $schema->getTable('sales_items');

    $integerType = Type::getType(Type::INTEGER);

    $usersPointsTable->getColumn('price')->setType($integerType);
    $usersPointsTable->getColumn('discount')->setType($integerType);
  }
}
