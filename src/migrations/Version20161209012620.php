<?php

namespace Acme\Migrations;

use App\Entities\Sale\Check\Type as CheckType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added sales table for short Check
 */
class Version20161209012620 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $salesTable = $schema->createTable('sales');
    $salesTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $salesTable->setPrimaryKey(['id']);
    $salesTable->addColumn('date', Type::DATETIME)->setNotnull(true);
    $salesTable->addColumn('count', Type::INTEGER)->setNotnull(false);
    $salesTable->addColumn('number', Type::INTEGER)->setNotnull(false);
    $salesTable->addColumn('till', Type::STRING)->setNotnull(false);
    $salesTable->addColumn('type', Type::STRING)->setNotnull(false);
    $salesTable->addColumn('sum', Type::FLOAT)->setNotnull(false);
    $salesTable->addColumn('operation_type', Type::STRING)->setNotnull(true);
    $salesTable->addColumn('point_id', Type::INTEGER)->setNotnull(true);
    $salesTable->addColumn('user_id', Type::INTEGER)->setNotnull(true);

    $salesTable
      ->addIndex(['date'], 'idx_sales_date')
      ->addIndex(['count'], 'idx_sales_count')
      ->addIndex(['number'], 'idx_sales_number')
      ->addIndex(['till'], 'idx_sales_till')
      ->addIndex(['type'], 'idx_sales_type')
      ->addIndex(['sum'], 'idx_sales_sum')
      ->addIndex(['operation_type'], 'idx_sales_operation_type')
      ->addIndex(['user_id'], 'idx_sales_user_id')
      ->addIndex(['point_id'], 'idx_sales_point_id')
      ->addIndex(['user_id', 'point_id'], 'idx_sales_user_id_point_id')
      ->addIndex(['id', 'date', 'count', 'sum', 'operation_type', 'user_id', 'point_id'], 'idx_sales_z_report')
      ->addIndex(['id', 'date', 'count', 'number', 'till', 'type', 'sum', 'operation_type', 'user_id', 'point_id'], 'idx_sales_short')
      ->addIndex(['id', 'date', 'count', 'number', 'till', 'type', 'operation_type', 'user_id', 'point_id'], 'idx_sales_full');

    $pointsTable = $schema->getTable('points');
    $pointsTable->addColumn('check_type', Type::STRING)->setDefault(CheckType::ZREPORT);
    $pointsTable->addIndex(['check_type'], 'idx_points_check_type');
  }

  public function postUp(Schema $schema) {
    $salesTable = $schema->getTable('sales');
    $salesTable->addForeignKeyConstraint('users', ['user_id'], ['id'], [], 'frg_sales_users_user_id_id');
    $salesTable->addForeignKeyConstraint('points', ['point_id'], ['id'], [], 'frg_sales_points_point_id_id');
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $salesTable = $schema->getTable('sales');
//    $salesTable->removeForeignKey('frg_sales_users_user_id_id');

    $salesTable->dropIndex('idx_sales_date');
    $salesTable->dropIndex('idx_sales_count');
    $salesTable->dropIndex('idx_sales_number');
    $salesTable->dropIndex('idx_sales_till');
    $salesTable->dropIndex('idx_sales_type');
    $salesTable->dropIndex('idx_sales_operation_type');
    $salesTable->dropIndex('idx_sales_user_id');
    $salesTable->dropIndex('idx_sales_point_id');
    $salesTable->dropIndex('idx_sales_z_report');
    $salesTable->dropIndex('idx_sales_short');

    $schema->dropTable('sales');
  }
}
