<?php

namespace Acme\Migrations;

use App\Entities\BlockedStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added status column in users table
 */
class Version20161209004746 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $usersTable = $schema->getTable('users');
    $usersTable->addColumn('status', Type::STRING)->setDefault(BlockedStatus::DEFAULT);
    $usersTable->addIndex(['status'], 'idx_users_status');
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $usersTable = $schema->getTable('users');
    $usersTable->dropIndex('idx_users_status');
    $usersTable->dropColumn('status');
  }
}
