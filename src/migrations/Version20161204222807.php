<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added groups table
 */
class Version20161204222807 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $groupsTable = $schema->createTable('groups');
    $groupsTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $groupsTable->setPrimaryKey(['id']);
    $groupsTable->addColumn('name', Type::STRING)->setLength(255);
    $groupsTable->addColumn('owner', Type::INTEGER);

    $groupsTable
      ->addIndex(['id'], 'idx_groups_id')
      ->addUniqueIndex(['name'], 'idx_groups_name');

    $usersTable = $schema->getTable('users');
    $usersTable->addColumn('group_id', Type::INTEGER)->setNotnull(true)->setDefault(0);
  }

  public function postUp(Schema $schema) {
    $usersTable = $schema->getTable('users');
    $usersTable->addForeignKeyConstraint('groups', ['group_id'], ['id'], [], 'users_group_id_frg');

    $groupsTable = $schema->getTable('groups');
    $groupsTable->addForeignKeyConstraint('users', ['owner'], ['id'], [], 'groups_owner_user_id_frg');

    $this->connection->insert('groups', [
      'id'    => 1,
      'name'  => 'main',
      'owner' => 1
    ]);

    $this->connection->update('users', [
      'group_id' => 1
    ], ['id' => 1]);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $usersTable = $schema->getTable('users');
    $usersTable->removeForeignKey('users_group_id_frg');
    $usersTable->dropColumn('group_id');

    $groupsTable = $schema->getTable('groups');
    $groupsTable->removeForeignKey('groups_owner_user_id_frg');
    $groupsTable->dropIndex('idx_groups_id');
    $groupsTable->dropIndex('idx_groups_name');

    $schema->dropTable('groups');
  }
}
