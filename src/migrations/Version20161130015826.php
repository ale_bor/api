<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added tokens table
 */
class Version20161130015826 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $table = $schema->createTable("tokens");
    $table->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $table->setPrimaryKey(['id']);
    $table->addColumn('token', Type::STRING)->setLength(255);
    $table->addColumn('user_id', Type::INTEGER);

    $table
      ->addIndex(['id'], 'idx_tokens_id')
      ->addIndex(['token'], 'idx_tokens_token');
  }

  public function postUp(Schema $schema) {
    $table = $schema->getTable("tokens");
    $table->addForeignKeyConstraint('users', ['user_id'], ['id'], [], 'user_id_frg');
  }


  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $table = $schema->getTable('tokens');
    $table->removeForeignKey('user_id_frg');
    $schema->dropTable('tokens');
  }
}
