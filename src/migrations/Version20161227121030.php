<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added days status table
 */
class Version20161227121030 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $daysTable = $schema->createTable('days');
    $daysTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $daysTable->setPrimaryKey(['id']);
    $daysTable->addColumn('date', Type::DATE);
    $daysTable->addColumn('status', Type::STRING);
    $daysTable->addColumn('point_id', Type::INTEGER);

    $daysTable->addIndex(['date'], 'idx_days_date');
    $daysTable->addIndex(['status'], 'idx_days_status');
    $daysTable->addIndex(['point_id'], 'idx_days_point_id');
    $daysTable->addIndex(['id', 'date', 'status', 'point_id'], 'idx_days_full');
  }

  public function postUp(Schema $schema) {
    $daysTable = $schema->getTable('days');
    $daysTable->addForeignKeyConstraint('points', ['point_id'], ['id']);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $daysTable = $schema->getTable('days');

    $daysTable->dropIndex('idx_days_date');
    $daysTable->dropIndex('idx_days_status');
    $daysTable->dropIndex('idx_days_point_id');
    $daysTable->dropIndex('idx_days_full');

    $schema->dropTable($daysTable);
  }
}
