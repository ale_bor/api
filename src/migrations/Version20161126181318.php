<?php

namespace Acme\Migrations;

use App\Entities\Role;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added users table
 */
class Version20161126181318 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $table = $schema->createTable('users');
    $table->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $table->setPrimaryKey(['id']);
    $table->addColumn('name', Type::STRING)->setNotnull(true);
    $table->addColumn('email', Type::STRING)->setNotnull(true);
    $table->addColumn('password', Type::STRING)->setNotnull(true);
    $table->addColumn('role', Type::STRING)->setDefault(Role::USER)->setNotnull(true);

    $table
      ->addIndex(['name'], 'idx_users_name')
      ->addUniqueIndex(['email'], 'idx_users_email')
      ->addIndex(['password'], 'idx_users_password')
      ->addIndex(['role'], 'idx_users_role')
      ->addIndex(['email', 'password'], 'idx_users_auth');
  }

  public function postUp(Schema $schema) {
    $this->connection->insert('users', [
      'id'       => 1,
      'name'     => 'Суперадминистратор',
      'email'    => '79253133196@yandex.ru',
      'password' => sha1('123456'),
      'role'     => Role::GOD
    ]);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $schema->dropTable('users');
  }
}
