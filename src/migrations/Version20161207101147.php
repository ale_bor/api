<?php

namespace Acme\Migrations;

use App\Entities\PointStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added points table
 */
class Version20161207101147 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $pointsTable = $schema->createTable('points');
    $pointsTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $pointsTable->setPrimaryKey(['id']);
    $pointsTable->addColumn('name', Type::STRING);
    $pointsTable->addColumn('owner', Type::INTEGER);
    $pointsTable->addColumn('room_number', Type::STRING);
    $pointsTable->addColumn('identifier', Type::STRING);
    $pointsTable->addColumn('status', Type::STRING)->setDefault(PointStatus::NEW);

    $pointsTable
      ->addUniqueIndex(['name'], 'idx_points_name')
      ->addIndex(['owner'], 'idx_points_owner')
      ->addIndex(['room_number'], 'idx_points_room_number')
      ->addIndex(['identifier'], 'idx_points_identifier')
      ->addIndex(['status'], 'idx_points_status');


    $usersPoints = $schema->createTable('users_points');
    $usersPoints->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $usersPoints->setPrimaryKey(['id']);
    $usersPoints->addColumn('point_id', Type::INTEGER);
    $usersPoints->addColumn('user_id', Type::INTEGER);
    $usersPoints->addColumn('write_access', Type::BOOLEAN);

    $usersPoints
      ->addUniqueIndex(['point_id', 'user_id'], 'idx_users_points_point_id_user_id')
      ->addIndex(['point_id'], 'idx_users_points_point_id')
      ->addIndex(['user_id'], 'idx_users_points_user_id')
      ->addIndex(['write_access'], 'idx_users_points_write_access');
  }

  public function postUp(Schema $schema) {
    $pointsTable = $schema->getTable('points');
    $pointsTable->addForeignKeyConstraint('users', ['owner'], ['id'], [], 'points_owner_user_id_frg');

    $usersPoints = $schema->getTable('users_points');
    $usersPoints->addForeignKeyConstraint('points', ['point_id'], ['id'], [], 'users_points_point_id_id_frg');
    $usersPoints->addForeignKeyConstraint('users', ['user_id'], ['id'], [], 'users_points_user_id_id_frg');


    $this->connection->insert('points', [
      'id'          => 1,
      'name'        => 'Объект 0',
      'owner'       => 1,
      'room_number' => '0',
      'identifier'  => '0',
      'status'      => PointStatus::NEW,
    ]);
    $this->connection->insert('users_points', [
      'id'           => 1,
      'point_id'     => 1,
      'user_id'      => 1,
      'write_access' => true,
    ]);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
  }
}
