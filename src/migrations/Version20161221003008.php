<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added full sale items table
 */
class Version20161221003008 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $salesItemsTable = $schema->createTable('sales_items');
    $salesItemsTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $salesItemsTable->setPrimaryKey(['id']);
    $salesItemsTable->addColumn('sale_id', Type::INTEGER);
    $salesItemsTable->addColumn('article', Type::STRING);
    $salesItemsTable->addColumn('name', Type::STRING);
    $salesItemsTable->addColumn('price', Type::INTEGER);
    $salesItemsTable->addColumn('amount', Type::INTEGER);
    $salesItemsTable->addColumn('discount', Type::INTEGER);

    $salesItemsTable
      ->addIndex(['sale_id'], 'idx_sales_items_sale_id')
      ->addIndex(['article'], 'idx_sales_items_article')
      ->addIndex(['name'], 'idx_sales_items_name')
      ->addIndex(['price'], 'idx_sales_items_price')
      ->addIndex(['amount'], 'idx_sales_items_amount')
      ->addIndex(['discount'], 'idx_sales_items_discount')
      ->addIndex(['sale_id', 'article', 'name', 'price', 'amount', 'discount'], 'idx_sales_items_all');
  }

  public function postUp(Schema $schema) {
    $salesItemsTable = $schema->getTable('sales_items');
    $salesItemsTable->addForeignKeyConstraint('sales', ['sale_id'], ['id']);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $salesItemsTable = $schema->getTable('sales_items');

    $salesItemsTable->dropIndex('idx_sales_items_sale_id');
    $salesItemsTable->dropIndex('idx_sales_items_article');
    $salesItemsTable->dropIndex('idx_sales_items_name');
    $salesItemsTable->dropIndex('idx_sales_items_price');
    $salesItemsTable->dropIndex('idx_sales_items_amount');
    $salesItemsTable->dropIndex('idx_sales_items_discount');
    $salesItemsTable->dropIndex('idx_sales_items_all');

    $schema->dropTable($salesItemsTable);
  }
}
