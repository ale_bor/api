<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added log table
 */
class Version20161226032738 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $logTable = $schema->createTable('log');
    $logTable->addColumn('id', Type::INTEGER, ["unsigned" => true])->setAutoincrement(true);
    $logTable->setPrimaryKey(['id']);
    $logTable->addColumn('date', Type::DATE)->setNotnull(true);
    $logTable->addColumn('time', Type::TIME)->setNotnull(true);
    $logTable->addColumn('action', Type::STRING)->setLength(65)->setNotnull(true);
    $logTable->addColumn('meta', Type::JSON_ARRAY)->setNotnull(false);
    $logTable->addColumn('user_id', Type::INTEGER)->setNotnull(true);
    $logTable->addColumn('point_id', Type::INTEGER)->setNotnull(false);

    $logTable->addIndex(['id'], 'idx_log_id');
    $logTable->addIndex(['date'], 'idx_log_date');
    $logTable->addIndex(['time'], 'idx_log_time');
    $logTable->addIndex(['action'], 'idx_log_action');
    $logTable->addIndex(['user_id'], 'idx_log_user_id');
    $logTable->addIndex(['point_id'], 'idx_log_point_id');
    $logTable->addIndex(['date', 'time'], 'idx_log_date_time');
    $logTable->addIndex(['id', 'date', 'time', 'action', 'user_id'], 'idx_log_without_point_id');
    $logTable->addIndex(['id', 'date', 'time', 'action', 'user_id', 'point_id'], 'idx_log_full');
  }

  public function postUp(Schema $schema) {
    $logTable = $schema->getTable('log');
    $logTable->addForeignKeyConstraint('users', ['user_id'], ['id']);
    $logTable->addForeignKeyConstraint('points', ['point_id'], ['id']);
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $logTable = $schema->getTable('log');

    $logTable->dropIndex('idx_log_id');
    $logTable->dropIndex('idx_log_date');
    $logTable->dropIndex('idx_log_time');
    $logTable->dropIndex('idx_log_action');
    $logTable->dropIndex('idx_log_user_id');
    $logTable->dropIndex('idx_log_point_id');
    $logTable->dropIndex('idx_log_date_time');
    $logTable->dropIndex('idx_log_without_point_id');
    $logTable->dropIndex('idx_log_full');

    $schema->dropTable($logTable);
  }
}
