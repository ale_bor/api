<?php
/**
 * (c) 2016 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added currency for point
 */
class Version20161228000720 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $pointsTable = $schema->getTable('points');
    $pointsTable->addColumn('currency', Type::STRING)->setLength(3)->setNotnull(true)->setDefault('RUB');
    $pointsTable->addIndex(['currency'], 'idx_points_currency');
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $pointsTable = $schema->getTable('points');
    $pointsTable->dropIndex('idx_points_currency');
    $pointsTable->dropColumn('currency');
  }
}
