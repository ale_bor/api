<?php

namespace Acme\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * Added user point input type
 */
class Version20170215122949 extends AbstractMigration {
  /**
   * @param Schema $schema
   */
  public function up(Schema $schema) {
    $usersPointsTable = $schema->getTable('users_points');
    $usersPointsTable->addColumn('input_type', Type::STRING)->setNotnull(true)->setDefault('HAND_AND_UPLOAD');
    $usersPointsTable->addIndex(['input_type'], 'idx_users_points_input_type');
  }

  /**
   * @param Schema $schema
   */
  public function down(Schema $schema) {
    $usersPointsTable = $schema->getTable('users_points');
    $usersPointsTable->dropIndex('idx_users_points_input_type');
    $usersPointsTable->dropColumn('input_type');
  }
}
