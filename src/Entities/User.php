<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class User
 * @package App\Entities
 * @ORM\Table(name="users")
 * @ORM\Entity()
 */
class User implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var string
   * @ORM\Column(type="text")
   */
  private $name;
  /**
   * @var string
   * @ORM\Column(type="text")
   */
  private $email;
  /**
   * @var string
   * @ORM\Column(type="text")
   */
  private $password;
  /**
   * @var string
   * @ORM\Column(type="text")
   */
  private $role = Role::USER;
  /**
   * @var string
   * @ORM\Column(type="text")
   * @Enum(value={"NEW", "DEFAULT", "BLOCKED"})
   */
  private $status;
  /**
   * @var Group
   * @ORM\OneToOne(targetEntity="Group")
   */
  private $group;
  /**
   * @var Token[]
   * @ORM\OneToMany(targetEntity="Token",mappedBy="user", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
   */
  private $tokens;
  /**
   * @var ArrayCollection|UserPoint[]
   * @ORM\OneToMany(targetEntity="UserPoint", mappedBy="user", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
   */
  private $points;
  /**
   * @var ArrayCollection
   * @ORM\OneToMany(targetEntity="Log", mappedBy="user")
   */
  private $actions;

  public function __construct() {
    $this->points = new ArrayCollection();
    $this->actions = new ArrayCollection();
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail(string $email) {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getPassword(): string {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword(string $password) {
    $this->password = $password;
  }

  /**
   * @return string
   */
  public function getRole(): string {
    return $this->role;
  }

  /**
   * @param string $role
   */
  public function setRole(string $role) {
    $this->role = $role;
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus(string $status) {
    $this->status = $status;
  }

  /**
   * @return Group
   */
  public function getGroup(): Group {
    return $this->group;
  }

  /**
   * @param Group $group
   */
  public function setGroup(Group $group) {
    $this->group = $group;
  }

  /**
   * @return Token[]
   */
  public function getTokens(): array {
    return $this->tokens;
  }

  /**
   * @return ArrayCollection|UserPoint[]
   */
  public function getPoints() {
    return $this->points;
  }

  /**
   * @return ArrayCollection
   */
  public function getActions(): ArrayCollection {
    return $this->actions;
  }

  /**
   * @param ArrayCollection $actions
   */
  public function setActions(ArrayCollection $actions) {
    $this->actions = $actions;
  }

  public function isUser(): bool {
    return $this->role == Role::USER;
  }

  public function isAdmin(): bool {
    return $this->role == Role::ADMIN;
  }

  public function isSuperAdmin(): bool {
    return $this->role == Role::SUPERADMIN;
  }

  public function isGod(): bool {
    return $this->role == Role::GOD;
  }

  public function isNotUser(): bool {
    return $this->isAdmin() || $this->isSuperAdmin() || $this->isGod();
  }

  public function isBlocked(): bool {
    return $this->status == BlockedStatus::BLOCKED;
  }

  public function canBeDelete(): bool {
    return $this->status == BlockedStatus::NEW;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'     => $this->id,
      'name'   => $this->name,
      'email'  => $this->email,
      'role'   => $this->role,
      'status' => $this->status,
      'group'  => $this->group,
      'points' => $this->points->getValues(),
    ];
  }
}
