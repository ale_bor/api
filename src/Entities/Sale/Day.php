<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities\Sale;

use App\Entities\Point;
use DateTime;
use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class Day
 * @package App\Entities\Sale
 * @ORM\Entity()
 * @ORM\Table(name="days")
 */
class Day implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var DateTime
   * @ORM\Column(type="date")
   */
  private $date;
  /**
   * @var string
   * @Enum(value={"SALE", "NOSALE", "CLOSED", "E_WORK", "BLOCKED"})
   * @ORM\Column(type="string")
   */
  private $status = "SALE";
  /**
   * @var boolean
   * @ORM\Column(type="boolean")
   */
  private $closed = false;
  /**
   * @var Point
   * @ORM\ManyToOne(targetEntity="App\Entities\Point", inversedBy="days")
   */
  private $point;

  /**
   * Day constructor.
   */
  public function __construct() {
    $date = new DateTime();
    $this->date = new DateTime($date->format('d-m-Y'));
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return DateTime
   */
  public function getDate(): DateTime {
    return $this->date;
  }

  /**
   * @param DateTime $date
   */
  public function setDate(DateTime $date) {
    $this->date = new DateTime($date->format('d-m-Y'));
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus(string $status) {
    $this->status = $status;
  }

  /**
   * @return bool
   */
  public function isClosed(): bool {
    return $this->closed;
  }

  /**
   * @param bool $closed
   */
  public function setClosed(bool $closed) {
    $this->closed = $closed;
  }

  /**
   * @return Point
   */
  public function getPoint(): Point {
    return $this->point;
  }

  /**
   * @param Point $point
   */
  public function setPoint(Point $point) {
    $this->point = $point;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'      => $this->id,
      'date'    => $this->date->format('d-m-Y H:i:s'),
      'status'  => $this->status,
      'closed'  => $this->isClosed(),
      'pointId' => $this->point->getId(),
    ];
  }
}

class DayStatus extends \App\Helpers\Enum {
  const NODATA = "NODATA";
  const SALE = "SALE";
  const NOSALE = "NOSALE";
  const CLOSED = "CLOSED";
  const BLOCKED = "BLOCKED";
}
