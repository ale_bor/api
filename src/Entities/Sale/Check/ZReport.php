<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities\Sale\Check;

use App\Entities\Point;
use App\Entities\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class ZReport
 * @package App\Entities\Check
 * @ORM\Entity()
 * @ORM\Table(name="sales")
 */
class ZReport implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var DateTime
   * @ORM\Column(type="datetime")
   */
  private $date;
  /**
   * @var int
   * @ORM\Column(type="integer")
   */
  private $count;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $type = Type::ZREPORT;
  /**
   * @var float
   * @ORM\Column(type="integer")
   */
  private $sum;
  /**
   * @var string
   * @ORM\Column(type="text", name="operation_type")
   */
  private $status;
  /**
   * @var User
   * @ORM\ManyToOne(targetEntity="App\Entities\User")
   */
  private $user;
  /**
   * @var Point
   * @ORM\ManyToOne(targetEntity="App\Entities\Point")
   */
  private $point;

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return DateTime
   */
  public function getDate(): DateTime {
    return $this->date;
  }

  /**
   * @param DateTime $date
   */
  public function setDate(DateTime $date) {
    $this->date = $date;
  }

  /**
   * @return int
   */
  public function getCount(): int {
    return $this->count;
  }

  /**
   * @param int $count
   */
  public function setCount(int $count) {
    $this->count = $count;
  }

  /**
   * @return float
   */
  public function getSum(): float {
    return $this->sum;
  }

  /**
   * @param float $sum
   */
  public function setSum(float $sum) {
    $this->sum = $sum;
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus(string $status) {
    $this->status = $status;
  }

  /**
   * @return User
   */
  public function getUser(): User {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user) {
    $this->user = $user;
  }

  /**
   * @return Point
   */
  public function getPoint(): Point {
    return $this->point;
  }

  /**
   * @param Point $point
   */
  public function setPoint(Point $point) {
    $this->point = $point;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'      => $this->id,
      'date'    => $this->date->format('d-m-Y H:i:s'),
      'count'   => $this->count,
      'type'    => $this->type,
      'sum'     => $this->sum,
      'status'  => $this->status,
      'userId'  => $this->user->getId(),
      'pointId' => $this->point->getId(),
    ];
  }
}
