<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities\Sale\Check;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class FullSaleItem
 * @package App\Entities\Check
 * @ORM\Entity()
 * @ORM\Table(name="sales_items")
 */
class FullSaleItem implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var Full
   * @ORM\ManyToOne(targetEntity="Full", inversedBy="sale")
   */
  private $sale;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $article;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $name;
  /**
   * @var float
   * @ORM\Column(type="decimal")
   */
  private $price;
  /**
   * @var int
   * @ORM\Column(type="integer")
   */
  private $amount;
  /**
   * @var float
   * @ORM\Column(type="decimal")
   */
  private $discount;

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return Full
   */
  public function getSale(): Full {
    return $this->sale;
  }

  /**
   * @param Full $sale
   */
  public function setSale(Full $sale) {
    $this->sale = $sale;
  }

  /**
   * @return string
   */
  public function getArticle(): string {
    return $this->article;
  }

  /**
   * @param string $article
   */
  public function setArticle(string $article) {
    $this->article = $article;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * @return float
   */
  public function getPrice(): float {
    return $this->price;
  }

  /**
   * @param float $price
   */
  public function setPrice(float $price) {
    $this->price = $price;
  }

  /**
   * @return int
   */
  public function getAmount(): int {
    return $this->amount;
  }

  /**
   * @param int $amount
   */
  public function setAmount(int $amount) {
    $this->amount = $amount;
  }

  /**
   * @return float
   */
  public function getDiscount(): float {
    return $this->discount;
  }

  /**
   * @param float $discount
   */
  public function setDiscount(float $discount) {
    $this->discount = $discount;
  }

  public function isValid(): bool {
    return !empty($this->getAmount())
      && !empty($this->getPrice())
      && !empty($this->getDiscount())
      && !empty($this->getName())
      && !empty($this->getArticle());
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'       => $this->id,
      'saleId'   => $this->sale ? $this->sale->getId() : 0,
      'article'  => $this->article,
      'name'     => $this->name,
      'price'    => $this->price,
      'amount'   => $this->amount,
      'discount' => $this->discount,
    ];
  }
}
