<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities\Sale\Check;

use App\Helpers\Enum;

class Type extends Enum {
  const ZREPORT = 'ZREPORT';
  const SHORT = 'SHORT';
  const FULL = 'FULL';
}
