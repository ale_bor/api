<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities\Sale\Check;

use App\Entities\Point;
use App\Entities\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class Full
 * @package App\Entities\Check
 * @ORM\Entity()
 * @ORM\Table(name="sales")
 */
class Full implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var DateTime
   * @ORM\Column(type="datetime")
   */
  private $date;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $number;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $till;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $type = Type::FULL;
  /**
   * @var string
   * @ORM\Column(type="string", name="operation_type")
   */
  private $operationType;
  /**
   * @var User
   * @ORM\ManyToOne(targetEntity="App\Entities\User")
   */
  private $user;
  /**
   * @var Point
   * @ORM\ManyToOne(targetEntity="App\Entities\Point")
   */
  private $point;
  /**
   * @ORM\OneToMany(targetEntity="FullSaleItem", mappedBy="sale", cascade={"persist", "remove", "merge"}, fetch="EAGER")
   */
  private $items;

  /**
   * Full constructor.
   */
  public function __construct() {
    $this->items = new ArrayCollection([]);
  }

  /**
   * @return int|null
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return DateTime
   */
  public function getDate(): DateTime {
    return $this->date;
  }

  /**
   * @param DateTime $date
   */
  public function setDate(DateTime $date) {
    $this->date = $date;
  }

  /**
   * @return string
   */
  public function getNumber(): string {
    return $this->number;
  }

  /**
   * @param string $number
   */
  public function setNumber(string $number) {
    $this->number = $number;
  }

  /**
   * @return string
   */
  public function getTill(): string {
    return $this->till;
  }

  /**
   * @param string $till
   */
  public function setTill(string $till) {
    $this->till = $till;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType(string $type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getOperationType(): string {
    return $this->operationType;
  }

  /**
   * @param string $operationType
   */
  public function setOperationType(string $operationType) {
    $this->operationType = $operationType;
  }

  /**
   * @return User
   */
  public function getUser(): User {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user) {
    $this->user = $user;
  }

  /**
   * @return Point
   */
  public function getPoint(): Point {
    return $this->point;
  }

  /**
   * @param Point $point
   */
  public function setPoint(Point $point) {
    $this->point = $point;
  }

  /**
   * @return mixed|ArrayCollection
   */
  public function getItems() {
    return $this->items;
  }

  public function getSum(): float {
    $sum = 0;
    /**
     * @var $item FullSaleItem
     */
    foreach ($this->items as $item) {
      $sum += $item->getDiscount() > 0 ? $item->getDiscount() : $item->getAmount() * $item->getPrice();
    }

    return $sum;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  function jsonSerialize() {
    return [
      'id'            => $this->id,
      'date'          => $this->date->format('d-m-Y H:i:s'),
      'number'        => $this->number,
      'till'          => $this->till,
      'type'          => $this->type,
      'operationType' => $this->operationType,
      'userId'        => $this->user ? $this->user->getId() : 0,
      'pointId'       => $this->point ? $this->point->getId() : 0,
      'items'         => $this->items->getValues(),
    ];
  }
}
