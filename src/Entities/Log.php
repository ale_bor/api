<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use App\Helpers\Enum;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class Log
 * @package App\Entities
 * @ORM\Entity()
 * @ORM\Table(name="log")
 */
class Log implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var DateTime
   * @ORM\Column(type="date")
   */
  private $date;
  /**
   * @var DateTime
   * @ORM\Column(type="time")
   */
  private $time;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $action;
  /**
   * @var array|object
   * @ORM\Column(type="json_array")
   */
  private $meta;
  /**
   * @var User
   * @ORM\ManyToOne(targetEntity="User", inversedBy="actions")
   */
  private $user;
  /**
   * @var Point
   * @ORM\ManyToOne(targetEntity="Point", inversedBy="actions")
   */
  private $point;

  /**
   * Log constructor.
   */
  public function __construct() {
    $date = new DateTime();
    $this->date = new DateTime($date->format('d-m-Y'));
    $this->time = new DateTime($date->format('H:i:s'));
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return DateTime
   */
  public function getDate(): DateTime {
    return new DateTime($this->date->format('d-m-Y') . ' ' . $this->time->format('H:i:s'));
  }

  public function getDateWithoutTime(): DateTime {
    return $this->date;
  }

  public function getTime(): DateTime {
    return $this->time;
  }

  /**
   * @param DateTime $date
   */
  public function setDate(DateTime $date) {
    $this->date = new DateTime($date->format('d-m-Y'));
    $this->time = new DateTime($date->format('H:i:s'));
  }

  /**
   * @return string
   */
  public function getAction(): string {
    return $this->action;
  }

  /**
   * @param string $action
   */
  public function setAction(string $action) {
    $this->action = $action;
  }

  /**
   * @return array|object
   */
  public function getMeta() {
    return $this->meta;
  }

  /**
   * @param array|object $meta
   */
  public function setMeta($meta) {
    $this->meta = $meta;
  }

  /**
   * @return User
   */
  public function getUser(): User {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user) {
    $this->user = $user;
  }

  /**
   * @return Point
   */
  public function getPoint(): Point {
    return $this->point;
  }

  /**
   * @param Point $point
   */
  public function setPoint(Point $point) {
    $this->point = $point;
  }

  /**
   * Fast log action with user and meta
   * @param EntityManager $em
   * @param string $action
   * @param User $user
   * @param array|object|null $meta
   * @return Log
   */
  public static function log(EntityManager $em, string $action, User $user, $meta = null): Log {
    $log = new Log();

    $log->setAction($action);
    $log->setUser($user);
    $log->setMeta($meta);

    $em->persist($log);
    $em->flush($log);

    return $log;
  }

  /**
   * Fast point log action with user and meta
   * @param EntityManager $em
   * @param string $action
   * @param User $user
   * @param Point $point
   * @param null $meta
   * @return Log
   */
  public static function byPoint(EntityManager $em, string $action, User $user, Point $point, $meta = null): Log {
    $log = new Log();

    $log->setAction($action);
    $log->setMeta($meta);
    $log->setUser($user);
    $log->setPoint($point);

    $em->persist($log);
    $em->flush($log);

    return $log;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'     => $this->id,
      'date'   => $this->date->format('d-m-Y') . ' ' . $this->time->format('H:i:s'),
      'action' => $this->action,
      'meta'   => $this->meta,
      'user'   => $this->user,
      'point'  => !empty($this->point) ? $this->point : null,
    ];
  }
}

class LogAction extends Enum {
  const CREATE_SALE = 'CREATE_SALE';
  const REMOVE_SALE = 'REMOVE_SALE';
  const CHANGE_SALE = 'CHANGE_SALE';

  const CREATE_USER = 'CREATE_USER';
  const CHANGE_USER = 'CHANGE_USER';
  const REMOVE_USER = 'REMOVE_USER';

  const CREATE_POINT = 'CREATE_POINT';
  const CHANGE_POINT = 'CHANGE_POINT';
  const REMOVE_POINT = 'REMOVE_POINT';

  const CREATE_GROUP = 'CREATE_GROUP';
  const CHANGE_GROUP = 'CHANGE_GROUP';
  const REMOVE_GROUP = 'REMOVE_GROUP';

  const PARSE_EXCEL = 'PARSE_EXCEL';
  const PARSE_EXCEL_STARTED = 'PARSE_EXCEL_STARTED';
  const PARSE_EXCEL_SUCCESS = 'PARSE_EXCEL_SUCCESS';
  const PARSE_EXCEL_FAILURE = 'PARSE_EXCEL_FAILURE';
  const PARSE_EXCEL_CANCELED = 'PARSE_EXCEL_CANCELED';
  const CREATE_EXCEL = 'CREATE_EXCEL';

  const AUTH = 'AUTH';
  const DE_AUTH = 'DE_AUTH';
}
