<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use App\Helpers\DateHelper;
use App\Helpers\Enum;
use DateTime;
use DateTimeZone;
use Exception;
use JsonSerializable;

class DefaultResult extends Exception implements JsonSerializable {
  public $status;
  /**
   * @var Meta
   */
  public $meta;
  public $data;
  /**
   * @var string
   */
  public $error;
  public $_d;

  /**
   * DefaultResult constructor.
   * @param $status
   * @param Meta $meta
   * @param $data
   * @param $error string
   * @param $_d
   */
  public function __construct($status = Status::ok, Meta $meta = null, $data = null, $error = null, $_d = null) {
    parent::__construct($error);
    $this->status = $status;
    if (empty($meta)) {
      $meta = Meta::n();
    }
    $this->meta = $meta;
    $this->data = $data;
    $this->error = $error;
    $this->_d = $_d;
  }

  /**
   * @param string $errorText
   * @param $_d
   * @param Meta $meta
   * @return DefaultResult
   */
  public static function error(string $errorText, $_d = null, Meta $meta = null) {
    return new DefaultResult(Status::error, empty($meta) ? Meta::n() : $meta, null, $errorText, $_d);
  }

  /**
   * @param Meta|null $meta
   * @param null $_d
   * @return DefaultResult
   */
  public static function notReceived(Meta $meta = null, $_d = null) {
    return DefaultResult::error("Переданы не все поля", $_d, $meta);
  }

  /**
   * @param $data
   * @param string $tokenString
   * @param $_d
   * @return DefaultResult
   */
  public static function success($data, string $tokenString = null, $_d = null) {
    if (empty($tokenString)) {
      return new DefaultResult(Status::ok, Meta::n(), $data, null, $_d);
    } else {
      return new DefaultResult(Status::ok, Meta::token($tokenString), $data, null, $_d);
    }
  }

  /**
   * @param $_d
   * @return DefaultResult
   */
  public static function empty($_d = null) {
    return new DefaultResult(Status::ok, Meta::n(), null, null, $_d);
  }

  /**
   * @param $_d
   * @return DefaultResult
   */
  public static function noAccess($_d = null) {
    return new DefaultResult(Status::noAccess, Meta::n(), null, null, $_d);
  }

  /**
   * @param $_d
   * @return DefaultResult
   */
  public static function authRequired($_d = null) {
    return new DefaultResult(Status::authRequired, Meta::n(), null, null, $_d);
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'status' => $this->status,
      'meta'   => $this->meta,
      'data'   => $this->data,
      'error'  => $this->error,
      '_d'     => $this->_d,
    ];
  }
}

class Status extends Enum {
  const ok = 'ok';
  const error = 'error';
  const authRequired = 'authRequired';
  const noAccess = 'noAccess';
}

class Meta implements JsonSerializable {
  public $date;
  public $tokenString;
  public $notReceived;
  public $timezone;

  /**
   * Meta constructor.
   * @param DateTime $date
   * @param string $tokenString
   * @param array $notReceived
   * @param DateTimeZone|null $timezone
   */
  public function __construct(DateTime $date, string $tokenString = null, array $notReceived = [], DateTimeZone $timezone = null) {
    $this->date = $date;
    $this->tokenString = $tokenString;
    $this->notReceived = $notReceived;
    if (empty($timezone)) {
      $timezone = new DateTimeZone(date_default_timezone_get());
    }
    $this->timezone = $timezone;
  }

  public static function n(): Meta {
    return new Meta(new DateTime());
  }

  public static function token($tokenString): Meta {
    return new Meta(new DateTime(), $tokenString);
  }

  public static function notReceived(array $notReceived): Meta {
    return new Meta(new DateTime(), null, $notReceived);
  }

  public function jsonSerialize() {
    return [
      'date'        => DateHelper::formatWithDateTime($this->date),
      'tokenString' => $this->tokenString,
      'notReceived' => $this->notReceived,
      'timezone'    => $this->timezone->getName(),
    ];
  }
}
