<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class UserPoint
 * @package App\Entities
 * @ORM\Table(name="users_points")
 * @ORM\Entity()
 */
class UserPoint implements JsonSerializable {
  /**
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   * @var int
   */
  public $id;
  /**
   * @ORM\OneToOne(targetEntity="Point")
   * @var Point
   */
  private $point;
  /**
   * @ORM\ManyToOne(targetEntity="User", inversedBy="points")
   * @var User
   */
  private $user;
  /**
   * @ORM\Column(type="boolean",name="write_access")
   * @var bool
   */
  private $write;
  /**
   * @var string
   * @Enum(value={"HAND", "UPLOAD", "HAND_AND_UPLOAD"})
   * @ORM\Column(name="input_type", type="string")
   */
  private $inputType;

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return Point
   */
  public function getPoint(): Point {
    return $this->point;
  }

  /**
   * @param Point $point
   */
  public function setPoint(Point $point) {
    $this->point = $point;
  }

  /**
   * @return User
   */
  public function getUser(): User {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user) {
    $this->user = $user;
  }

  /**
   * @return boolean
   */
  public function isWrite(): bool {
    return $this->write;
  }

  /**
   * @param boolean $write
   */
  public function setWrite(bool $write) {
    $this->write = $write;
  }

  /**
   * @return string
   */
  public function getInputType(): string {
    return $this->inputType;
  }

  /**
   * @param string $inputType
   */
  public function setInputType(string $inputType) {
    $this->inputType = $inputType;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'         => $this->point->getId(),
      'point'      => $this->point->getId(),
      'name'       => $this->point->getName(),
      'roomNumber' => $this->point->getRoomNumber(),
      'userId'     => $this->user->getId(),
      'write'      => $this->write,
      'currency'   => $this->point->getCurrency(),
      'checkType'  => $this->point->getCheckType(),
      'inputType'  => $this->inputType,
      'status'     => $this->point->getStatus(),
    ];
  }
}
