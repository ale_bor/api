<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Token
 * @package App\Entities
 * @ORM\Entity()
 * @ORM\Table(name="tokens")
 */
class Token {
  /**
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   * @var int
   */
  private $id;
  /**
   * @ORM\Column(type="string")
   * @var string
   */
  private $token;
  /**
   * @ORM\ManyToOne(targetEntity="User")
   * @var User
   */
  private $user;

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getToken(): string {
    return $this->token;
  }

  /**
   * @param string $token
   */
  public function setToken(string $token) {
    $this->token = $token;
  }

  /**
   * @return User
   */
  public function getUser(): User {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user) {
    $this->user = $user;
  }
}
