<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class Group
 * @package App\Entities
 * @ORM\Table(name="groups")
 * @ORM\Entity()
 */
class Group implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $name;
  /**
   * @var User
   * @ORM\ManyToOne(targetEntity="User")
   * @ORM\JoinColumn(name="owner", referencedColumnName="id")
   */
  private $owner;

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * @return User
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * @param User $owner
   */
  public function setOwner($owner) {
    $this->owner = $owner;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'id'    => $this->id,
      'name'  => $this->name,
      'owner' => $this->owner->getId(),
    ];
  }
}
