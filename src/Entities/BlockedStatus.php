<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use App\Helpers\Enum;

class BlockedStatus extends Enum {
  const NEW = 'NEW';
  const DEFAULT = 'DEFAULT';
  const BLOCKED = 'BLOCKED';
}
