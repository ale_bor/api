<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Entities;

use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class Point
 * @package App\Entities
 * @ORM\Entity()
 * @ORM\Table(name="points")
 */
class Point implements JsonSerializable {
  /**
   * @var int
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue()
   */
  private $id;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $name;
  /**
   * @var User
   * @ORM\ManyToOne(targetEntity="User")
   * @ORM\JoinColumn(name="owner", referencedColumnName="id")
   */
  private $owner;
  /**
   * @var string
   * @ORM\Column(type="string",name="room_number")
   */
  private $roomNumber;
  /**
   * @var string
   * @ORM\Column(type="string")
   */
  private $identifier;
  /**
   * @var string
   * @ORM\Column(type="string")
   * @Enum(value={"ACTIVE","DELETED","CLOSED","BLOCKED","NEW"})
   */
  private $status;
  /**
   * @var ArrayCollection
   * @ORM\OneToMany(targetEntity="Log", mappedBy="point")
   */
  private $actions;
  /**
   * @var ArrayCollection
   * @ORM\OneToMany(targetEntity="App\Entities\Sale\Day", mappedBy="point", fetch="LAZY")
   */
  private $days;
  /**
   * @var string
   * @ORM\Column(type="text", name="check_type")
   */
  private $checkType;
  /**
   * @var string
   * @ORM\Column(type="string", nullable=false, length=3)
   * @Enum(value={"RUB", "USD", "EUR", "KZT"})
   */
  private $currency = "RUB";

  /**
   * Point constructor.
   */
  public function __construct() {
    $this->actions = new ArrayCollection();
    $this->days = new ArrayCollection();
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * @return User
   */
  public function getOwner(): User {
    return $this->owner;
  }

  /**
   * @param User $owner
   */
  public function setOwner(User $owner) {
    $this->owner = $owner;
  }

  /**
   * @return string
   */
  public function getRoomNumber(): string {
    return $this->roomNumber;
  }

  /**
   * @param string $roomNumber
   */
  public function setRoomNumber(string $roomNumber) {
    $this->roomNumber = $roomNumber;
  }

  /**
   * @return string
   */
  public function getIdentifier(): string {
    return $this->identifier;
  }

  /**
   * @param string $identifier
   */
  public function setIdentifier(string $identifier) {
    $this->identifier = $identifier;
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus(string $status) {
    $this->status = $status;
  }

  /**
   * @return ArrayCollection
   */
  public function getActions(): ArrayCollection {
    return $this->actions;
  }

  /**
   * @param ArrayCollection $actions
   */
  public function setActions(ArrayCollection $actions) {
    $this->actions = $actions;
  }

  /**
   * @return ArrayCollection
   */
  public function getDays(): ArrayCollection {
    return $this->days;
  }

  /**
   * @param ArrayCollection $days
   */
  public function setDays(ArrayCollection $days) {
    $this->days = $days;
  }

  /**
   * @return string
   */
  public function getCheckType(): string {
    return $this->checkType;
  }

  /**
   * @param string $checkType
   */
  public function setCheckType(string $checkType) {
    $this->checkType = $checkType;
  }

  /**
   * @return string
   */
  public function getCurrency(): string {
    return $this->currency;
  }

  /**
   * @param string $currency
   */
  public function setCurrency(string $currency) {
    $this->currency = $currency;
  }

  public function canBeDelete(): bool {
    return $this->status == PointStatus::NEW;
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  function jsonSerialize() {
    return [
      'id'         => $this->id,
      'name'       => $this->name,
      'owner'      => $this->owner->getId(),
      'roomNumber' => $this->roomNumber,
      'identifier' => $this->identifier,
      'status'     => $this->status,
      'checkType'  => $this->checkType,
      'currency'   => $this->currency,
    ];
  }
}
