<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Console;

use Knp\Command\Command;
use PHPExcel_CachedObjectStorageFactory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class InstallCommand extends Command {
  const CONFIG_DIR = __DIR__ . '/../config';
  const DB_CONFIG_FILE = self::CONFIG_DIR . '/db.yml';
  const MAIL_CONFIG_FILE = self::CONFIG_DIR . '/mail.yml';
  const APP_CONFIG_FILE = self::CONFIG_DIR . '/app.yml';
  const EXCEL_CONFIG_FILE = self::CONFIG_DIR . '/excel.yml';

  /**
   * InstallCommand constructor.
   */
  public function __construct() {
    parent::__construct('install');
  }

  protected function configure() {
    $this->setDescription('Позволяет установить и сконфигурировать приложение');

    $this->addOption('debug', null, InputOption::VALUE_OPTIONAL, 'Установить в режиме разработчика', false);

    //Database
    $this->addOption('db.dbname', null, InputOption::VALUE_OPTIONAL, 'Имя базы данных', 'rvdp');
    $this->addOption('db.host', null, InputOption::VALUE_OPTIONAL, 'Хост', 'localhost');
    $this->addOption('db.user', null, InputOption::VALUE_REQUIRED, 'Пользователь');
    $this->addOption('db.password', null, InputOption::VALUE_REQUIRED, 'Пароль');

    //Mail
    $this->addOption('mail.host', null, InputOption::VALUE_OPTIONAL, 'Хост сервера отправки почты', 'smtp.yandex.ru');
    $this->addOption('mail.port', null, InputOption::VALUE_OPTIONAL, 'Порт сервера отправки почты', 465);
    $this->addOption('mail.encryption', null, InputOption::VALUE_OPTIONAL, 'Тип шифрования', 'SSL');
    $this->addOption('mail.username', null, InputOption::VALUE_REQUIRED, 'Логин');
    $this->addOption('mail.password', null, InputOption::VALUE_REQUIRED, 'Пароль');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    if (file_exists(self::CONFIG_DIR)) {
      $output->writeln("Система уже установлена");

      return;
    }

    if (!file_exists(self::CONFIG_DIR) || !is_dir(self::CONFIG_DIR)) {
      if (!mkdir(self::CONFIG_DIR)) {
        $output->writeln("Не удалось создать директорию " . self::CONFIG_DIR);
      }
    }

    $db = [
      'db.options' => [
        'driver'   => 'pdo_mysql',
        'dbname'   => $input->getOption('db.dbname'),
        'host'     => $input->getOption('db.host'),
        'user'     => $input->getOption('db.user'),
        'password' => $input->getOption('db.password'),
      ],
    ];

    file_put_contents(self::DB_CONFIG_FILE, Yaml::dump($db));

    $mail = [
      'host'       => $input->getOption('mail.host'),
      'port'       => $input->getOption('mail.port'),
      'encryption' => $input->getOption('mail.encryption'),
      'username'   => $input->getOption('mail.username'),
      'password'   => $input->getOption('mail.password'),
      'auth_mode'  => 'login',
    ];

    file_put_contents(self::MAIL_CONFIG_FILE, Yaml::dump($mail));

    $excel = [
      'cache' => [
        'method'   => PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp,
        'settings' => [
          'memoryCacheSize' => '256MB',
        ],
      ],
    ];

    file_put_contents(self::EXCEL_CONFIG_FILE, Yaml::dump($excel));

    file_put_contents(self::APP_CONFIG_FILE, Yaml::dump([
      'debug'       => mb_strtolower($input->getOption('debug')) == 'true',
      'api.version' => 1,
    ]));

    $output->writeln('Теперь вы можете запустить миграцию. bin/migrations migrations:migrate');
  }

}
