<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Providers;

use App\Entities\Point;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class PointsServiceProvider implements ServiceProviderInterface, BootableProviderInterface {
  /**
   * @var EntityManager
   */
  private $em;
  /**
   * @var EntityRepository
   */
  private $pointRepository;

  /**
   * Registers services on the given container.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   *
   * @param Container $pimple A container instance
   */
  public function register(Container $pimple) {
    $pimple['points_service_provider'] = $this;
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot(Application $app) {
    $this->em = $app['orm.em'];
    $this->pointRepository = $this->em->getRepository(Point::class);
  }

  /**
   * @param int $page
   * @param int $limit
   * @return Point[]
   */
  public function getAll(int $page = 0, int $limit = 50) {
    return $this->pointRepository->findBy([], null, $limit, $page * $limit);
  }

  /**
   * @param int $id
   * @return Point|null|object
   */
  public function getPoint(int $id) {
    return $this->pointRepository->find($id);
  }

  /**
   * @param int $userId
   * @return Point[]
   */
  public function getByUser(int $userId) {
    return $this->pointRepository->findBy(['user_id' => $userId]);
  }

  /**
   * @param Point $point
   * @return Point|bool
   */
  public function create(Point $point) {
    $this->em->persist($point);
    $this->em->flush($point);

    return $point;
  }

  /**
   * @param Point $point
   * @return Point|bool
   */
  public function update(Point $point) {
    $this->em->flush($point);

    return $point;
  }

  /**
   * @param Point $point
   */
  public function delete(Point $point) {
    $this->em->remove($point);
    $this->em->flush($point);
  }
}
