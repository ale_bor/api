<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Providers;

use App\Entities\BlockedStatus;
use App\Entities\DefaultResult;
use App\Entities\Point;
use App\Entities\Role;
use App\Entities\Token;
use App\Entities\User;
use App\Entities\UserPoint;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class UsersServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

  /**
   * @var EntityManager
   */
  private $em;
  /**
   * @var EntityRepository
   */
  private $userRepository;
  /**
   * @var EntityRepository
   */
  private $tokenRepository;

  /**
   * Registers services on the given container.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   *
   * @param Container $pimple A container instance
   */
  public function register(Container $pimple) {
    $pimple['users_service_provider'] = $this;
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot(Application $app) {
    $this->em = $app['orm.em'];
    $this->userRepository = $this->em->getRepository(User::class);
    $this->tokenRepository = $this->em->getRepository(Token::class);
  }

  /**
   * @param int $id
   * @return User|null|object
   */
  public function getUser(int $id) {
    return $this->userRepository->find($id);
  }

  /**
   * @param string $email
   * @return User|null|object
   */
  public function getByEmail(string $email) {
    return $this->userRepository->findOneBy(['email' => $email]);
  }

  /**
   * @param string $tokenString
   * @return User|bool
   */
  public function getByToken(string $tokenString) {
    /**
     * @var Token $token
     */
    if ($token = $this->tokenRepository->findOneBy(['token' => $tokenString])) {
      return $token->getUser();
    }

    return false;
  }

  /**
   * @param Point $point
   * @return User[]
   */
  public function getByPoint(Point $point) {
    /**
     * @var $userPoints UserPoint[]
     */
    $userPoints = $this->em->getRepository(UserPoint::class)->findBy(['point' => $point]);

    $users = [];

    foreach ($userPoints as $userPoint) {
      $users[] = $userPoint->getUser();
    }

    return array_unique($users);
  }

  /**
   * @param int $id
   * @param string $password
   * @return bool
   */
  public function changePassword(int $id, string $password) {
    if (empty($password) || strlen($password) < 6) {
      return false;
    }
    $user = $this->getUser($id);
    $user->setPassword($password);

    return $this->update($user) !== false;
  }

  /**
   * @param User $user
   * @return User
   */
  public function create(User $user) {
    $this->em->persist($user);
    $this->em->flush($user);

    return $user;
  }

  /**
   * @param User $user
   * @return User
   */
  public function update(User $user) {
    $this->em->flush($user);

    return $user;
  }

  public function delete(User $user) {
    $this->em->remove($user);
    $this->em->flush($user);
  }

  /**
   * @param int $groupId
   * @param int $page
   * @param int $limit
   * @return User[]
   */
  public function getByGroup(int $groupId, int $page = 0, int $limit = 50) {
    return $this->userRepository->findBy(['group_id' => $groupId], null, $limit, $page * $limit);
  }

  /**
   * @param int $page
   * @param int $limit
   * @return User[]
   */
  public function getAll(int $page = 0, int $limit = 50) {
    return $this->userRepository->findBy([], null, $limit, $page * $limit);
  }

  /**
   * @param User $user
   * @return Token
   */
  public function generateToken(User $user) {
    $random255Bytes = openssl_random_pseudo_bytes(64);
    $hexString = bin2hex($random255Bytes);

    $token = new Token();
    $token->setUser($user);
    $token->setToken($hexString);

    $this->em->persist($token);
    $this->em->flush();

    return $token;
  }

  /**
   * @param string $tokenString
   * @return bool
   */
  public function removeToken(string $tokenString) {
    /**
     * @var $token Token
     */
    $token = $this->tokenRepository->findOneBy(['token' => $tokenString]);
    if (!empty($token)) {
      $this->em->remove($token);
      $this->em->flush();
    }

    return !empty($token);
  }

  /**
   * @param int $userId
   * @return bool
   */
  public function removeTokens(int $userId) {
    $tokens = $this->tokenRepository->findBy(['user_id' => $userId]);
    foreach ($tokens as $token) {
      $this->em->remove($token);
    }
    $this->em->flush();

    return count($tokens) > 0;
  }

  /**
   * @param string $email
   * @param string $password
   * @return User|null|object
   */
  public function validateLoginPassword($email, $password) {
    $user = $this->userRepository->findOneBy(['email' => $email, 'password' => self::securePassword($password)]);

    return $user;
  }

  /**
   * @param string $password
   * @return string
   */
  public static function securePassword(string $password) {
    return sha1($password);
  }

  /**
   * @param Request $request
   * @param string $action
   * @return User|bool
   * @throws DefaultResult
   */
  public function iCanDoThis(Request $request, string $action) {
    $userPermissions = [
      //users
      ['get', 'users/{id}'],
      ['put', 'users/{id}'],
      ['delete', 'auth/{token}'],
      //days
      ['get', 'days/digest/{startDate}/{endDate}'],
      ['get', 'days/{pointId}/{date}/status'],
      ['put', 'days/status'],
      ['delete', 'days/{pointId}/{date}'],
      //excel
      ['get', 'download/{pointId}/{file}'],
      ['post', 'sales/upload/v2/{pointId}'],
      ['patch', 'sales/upload/v2/{session}/{sha1}'],
      ['delete', 'sales/upload/v2/{session}/{sha1}'],
      //sales
      ['get', 'sales/{pointId}/{type}/{startDate}/{endDate}'],
      ['put', 'sales/{type}'],
      ['post', 'sales/{type}'],
      ['delete', 'sales/{id}'],
    ];

    $adminPermissions = $userPermissions + [
        ['get', 'users'],
        ['put', 'users'],
        ['get', 'users/{id}'],
        ['delete', 'users/{id}'],
        ['put', 'users/{id}'],
        ['put', 'users/{id}/password'],
        ['get', 'groups'],
        ['get', 'logs/{startDate}/{endDate}'],
      ];

    $superadminPermissions = $adminPermissions + [];

    $token = $request->get('token');

    if (empty($token)) {
      throw DefaultResult::authRequired();
    }

    /**
     * @var $user User
     */
    if ($user = $this->getByToken($token)) {
      if ($user->getStatus() == BlockedStatus::BLOCKED) {
        throw DefaultResult::noAccess();
      }

      $method = strtolower($request->getMethod());

      function check($permissions, $method, $action) {
        foreach ($permissions as $permission) {
          if ($permission[0] == $method && $permission[1] == $action) {
            return true;
          }
        }

        return false;
      }

      switch ($user->getRole()) {
        case Role::USER:
          return check($userPermissions, $method, $action) ? $user : false;
          break;
        case Role::ADMIN:
          return check($adminPermissions, $method, $action) ? $user : false;
          break;
        case Role::SUPERADMIN:
          return check($superadminPermissions, $method, $action) ? $user : false;
          break;
        case Role::GOD:
          return $user;
          break;
        default:
          throw DefaultResult::noAccess();
      }
    }

    throw DefaultResult::authRequired();
  }
}
