<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Providers;

use App\Entities\Log;
use App\Entities\Point;
use App\Entities\PointStatus;
use App\Entities\Sale\Check\Full;
use App\Entities\Sale\Check\FullSaleItem;
use App\Entities\Sale\Check\Type;
use App\Entities\Sale\Check\ZReport;
use App\Entities\Sale\Day;
use App\Entities\User;
use DateTime;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PDO;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class SalesServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

  /**
   * @var EntityManager
   */
  private $em;
  /**
   * @var EntityRepository
   */
  private $userRepository;
  /**
   * @var Connection
   */
  private $db;

  /**
   * Registers services on the given container.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   *
   * @param Container $pimple A container instance
   */
  public function register(Container $pimple) {
    $pimple['sales_service_provider'] = $this;
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot(Application $app) {
    $this->db = $app['db'];
    $this->db->setFetchMode(PDO::FETCH_OBJ);

    $this->em = $app['orm.em'];
    $this->userRepository = $this->em->getRepository(User::class);
  }

  /**
   * @param string $type
   * @return EntityRepository
   */
  private function em($type): EntityRepository {
    $entryName = Full::class;
    switch ($type) {
      case Type::ZREPORT:
        $entryName = ZReport::class;
        break;
      case Type::FULL:
        $entryName = Full::class;
        break;
    }

    return $this->em->getRepository($entryName);
  }

  /**
   * @param string $type
   * @param int $pointId
   * @param int $page
   * @param int $limit
   * @return Full[]|ZReport[]
   */
  public function getAll(string $type, int $pointId, int $page = 0, int $limit = 50) {
    return $this->em($type)->findBy(['pointId' => $pointId], ['date'], $limit, $page * $limit);
  }

  /**
   * @param int $fullId
   * @return FullSaleItem[]
   */
  public function getFullSaleItems(int $fullId) {
    return $this->em->getRepository(FullSaleItem::class)->findBy(['sale_id' => $fullId]);
  }

  /**
   * @param string $type
   * @param int $pointId
   * @param DateTime $startDate
   * @param DateTime $endDate
   * @return Full[]|ZReport[]
   */
  public function getByInterval(string $type, int $pointId, DateTime $startDate, DateTime $endDate) {
    $stm = $this->db->prepare("SELECT id FROM sales WHERE type = :type AND point_id = :pointId AND date >= :from AND date <= :to ORDER BY date");
    $stm->bindValue('type', $type);
    $stm->bindValue('pointId', $pointId);
    $stm->bindValue('from', $startDate->format('Y-m-d H:i:s'));
    $stm->bindValue('to', $endDate->format('Y-m-d H:i:s'));
    if ($stm->execute()) {
      $ids = $stm->fetchAll(PDO::FETCH_COLUMN);
      $entityRepository = $this->em($type);
      $sales = $entityRepository->findBy(['id' => $ids]);

      return $sales;
    }

    return [];
  }

  /**
   * @param string $type
   * @param DateTime $date
   * @param int $pointId
   * @return int
   */
  public function getSalesCount(string $type, DateTime $date, int $pointId) {
    $stm = $this->db->prepare("SELECT COUNT(id) AS count FROM sales WHERE type = :type AND date(date) = :date AND point_id = :point");
    $stm->bindValue('type', $type);
    $stm->bindValue('date', $date->format('Y-m-d'));
    $stm->bindValue('point', $pointId);
    if ($stm->execute()) {
      return intval($stm->fetch()->count);
    }

    return 0;
  }

  /**
   * @param string $type
   * @param DateTime $date
   * @param int $pointId
   * @return int
   */
  public function getSalesSum(string $type, DateTime $date, int $pointId) {
    $sum = 0;
    $statement = $this->db->prepare("SELECT id FROM sales WHERE DATE(date) = :date AND point_id = :point AND type = :type");
    $statement->bindValue('type', $type);
    $statement->bindValue('date', $date->format('Y-m-d'));
    $statement->bindValue('point', $pointId);
    if ($statement->execute()) {
      foreach ($statement->fetchAll() as $item) {
        /**
         * @var $sale ZReport|Full
         */
        if ($sale = $this->em($type)->find($item->id)) {
          $sum += $sale->getSum();
        }
      }
    }

    return $sum;
  }

  /**
   * @param Point $point
   * @param DateTime $date
   * @return Day|null|object
   */
  public function day(Point $point, DateTime $date) {
    return $this->em->getRepository(Day::class)->findOneBy(['date' => $date, 'point' => $point]);
  }

  /**
   * @param Day $day
   * @return Day
   */
  public function createDay(Day $day) {
    $this->em->persist($day);
    $this->em->flush($day);

    return $day;
  }

  /**
   * @param Day $day
   * @return Day
   */
  public function updateDay(Day $day) {
    $this->em->flush($day);

    return $day;
  }

  /**
   * @param int $pointId
   * @param DateTime $date
   * @param string $action
   * @return string
   */
  public function getStatus(int $pointId, DateTime $date, string $action) {
    $stm = $this->db->prepare(/** @lang MySQL */
      "SELECT id FROM log WHERE date(date) <= :date AND point_id = :pointId AND action = :action ORDER BY date DESC, time DESC LIMIT 1");
    $params = ['date' => $date->format('Y-m-D'), 'pointId' => $pointId, 'action' => $action];
    if ($stm->execute($params) && $res = $stm->fetch()) {
      $log = $this->em->getRepository(Log::class)->find($res->id);
      if (!empty($log)) {
        $meta = $log->getMeta();

        return $meta['status'];
      }
    }

    return PointStatus::NEW;
  }

  /**
   * @param string|null $type
   * @param int $id
   * @return Full|ZReport|null|object
   */
  public function getById($type, int $id) {
    return $this->em($type)->find($id);
  }

  /**
   * @param $sale Full|ZReport
   * @return Full|ZReport
   */
  public function create($sale) {
    $this->em->persist($sale);
    $this->em->flush($sale);

    return $sale;
  }

  public function delete(int $id) {
    $stm = $this->db->prepare("SELECT type FROM sales WHERE id = :id LIMIT 1");
    $stm->bindValue('id', $id);
    if ($stm->execute() && $type = $stm->fetch()->type) {
      $typeClass = $type == Type::ZREPORT ? ZReport::class : Full::class;
      $sale = $this->em->find($typeClass, $id);
      if ($typeClass == Type::FULL) {
        $fullItemsRepository = $this->em->getRepository(FullSaleItem::class);
        foreach ($fullItemsRepository->findBy(['sale' => $sale]) as $item) {
          $this->em->remove($item);
          $this->em->flush($item);
          unset($item);
        }
      }
      $this->em->remove($sale);
      $this->em->flush($sale);
    }
  }

  /**
   * @param $sale Full|ZReport
   * @return Full|ZReport
   */
  public function update($sale) {
    $this->em->flush($sale);

    return $sale;
  }

  /**
   * @param string $type
   * @param DateTime $date
   * @param Point $point
   * @return Full[]|ZReport[]|null
   */
  public function getByDate(string $type, DateTime $date, Point $point) {
    return $this->em($type)->findBy(['point' => $point, 'date' => $date]);
  }

  /**
   * @param string $type
   * @param int $pointId
   * @param DateTime $startDate
   * @param DateTime $endDate
   */
  public function deleteByDate(string $type, int $pointId, DateTime $startDate, DateTime $endDate) {
    $sales = $this->getByInterval($type, $pointId, $startDate, $endDate);

    foreach ($sales as $sale) {
      $this->delete($sale->getId());
    }
  }

}
