<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Providers;

use App\Entities\Log;
use DateTime;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class LogServiceProvider implements ServiceProviderInterface, BootableProviderInterface {

  /**
   * @var EntityManager
   */
  private $em;
  /**
   * @var EntityRepository
   */
  private $logRepository;
  /**
   * @var Connection
   */
  private $db;

  /**
   * Registers services on the given container.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   *
   * @param Container $pimple A container instance
   */
  public function register(Container $pimple) {
    $pimple['logs_service_provider'] = $this;
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot(Application $app) {
    $this->db = $app['db'];
    $this->em = $app['orm.em'];
    $this->logRepository = $this->em->getRepository(Log::class);
  }

  /**
   * @param DateTime $startDate
   * @param DateTime $endDate
   *
   * @return Log[]
   */
  public function getByInterval(DateTime $startDate, DateTime $endDate) {
    $stm = $this->db->prepare("SELECT id FROM log WHERE date >= :from AND date <= :to");
    $stm->bindValue('from', $startDate->format('Y-m-d H:i:s'));
    $stm->bindValue('to', $endDate->format('Y-m-d H:i:s'));
    if ($stm->execute()) {
      $idContainers = $stm->fetchAll();
      $logs = [];
      foreach ($idContainers as $idContainer) {
        $logs[] = $this->logRepository->find($idContainer->id);
      }

      return $logs;
    }

    return [];
  }

}
