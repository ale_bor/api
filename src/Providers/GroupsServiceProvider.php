<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Providers;

use App\Entities\Group;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class GroupsServiceProvider implements ServiceProviderInterface, BootableProviderInterface {
  /**
   * @var EntityManager
   */
  private $em;
  /**
   * @var EntityRepository
   */
  private $groupRepository;

  /**
   * Registers services on the given container.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   *
   * @param Container $pimple A container instance
   */
  public function register(Container $pimple) {
    $pimple['groups_service_provider'] = $this;
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot(Application $app) {
    $this->em = $app['orm.em'];
    $this->groupRepository = $this->em->getRepository(Group::class);
  }

  /**
   * @param int $id
   * @return Group|null|object
   */
  public function getById(int $id) {
    return $this->groupRepository->find($id);
  }

  /**
   * @param int $ownerId
   * @return Group|null|object
   */
  public function getByOwner(int $ownerId) {
    return $this->groupRepository->findOneBy(['owner_id' => $ownerId]);
  }

  /**
   * @param Group $group
   * @return bool|Group
   */
  public function create(Group $group) {
    $this->em->persist($group);
    $this->em->flush($group);

    return $group;
  }

  /**
   * @param Group $group
   * @return Group
   */
  public function update(Group $group) {
    $this->em->flush($group);

    return $group;
  }

  /**
   * @param int $id
   * @return bool
   */
  public function delete(int $id) {
    $group = $this->groupRepository->find($id);
    $notEmpty = !empty($group);
    if ($notEmpty) {
      $this->em->remove($group);
      $this->em->flush($group);
    }

    return $notEmpty;
  }

  /**
   * @param int $page
   * @param int $limit
   * @return \App\Entities\Group[]
   */
  public function getAll(int $page = 0, int $limit = 50) {
    return $this->groupRepository->findBy([], null, $limit, $page * $limit);
  }
}
