<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use JDesrosiers\Silex\Provider\CorsServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;
use Symfony\Component\Yaml\Yaml;

define('EXCEL_TEMP_DIR', __DIR__ . '/../excel/');
define("APCU_ENABLED", function_exists('apcu_exists'));

require_once __DIR__ . '/../vendor/autoload.php';

$appConfigFile = __DIR__ . '/config/app.yml';

if (!appInstalled($appConfigFile)) {
  echo 'Необходимо установить систему';

  return;
}

$app = new Silex\Application();
$appConfig = loadConfigWithCache($appConfigFile);

$app['apiVersion'] = $appConfig['api.version'];
$app['basePath'] = "/v" . $appConfig['api.version'];

$app['debug'] = $appConfig['debug'];

$dbConfig = loadConfigWithCache(__DIR__ . '/config/db.yml');
$app->register(new DoctrineServiceProvider(), $dbConfig);

$app->register(new DoctrineOrmServiceProvider(), [
  'orm.proxies_dir'           => __DIR__ . '/../proxies',
  'orm.proxies_namespace'     => 'DoctrineProxy',
  'orm.auto_generate_proxies' => true,
  'orm.em.options'            => [
//    'metadata_cache' => 'apc',
    'mappings' => [
      [
        'type'                         => 'annotation',
        'namespace'                    => 'App\Entities',
        'path'                         => __DIR__ . '/Entities',
        'use_simple_annotation_reader' => false,
      ],
    ],
  ],
]);

$mailConfig = loadConfigWithCache(__DIR__ . '/config/mail.yml');
$app->register(new SwiftmailerServiceProvider());
$app['swiftmailer.options'] = $mailConfig;

$excelConfig = loadConfigWithCache(__DIR__ . '/config/excel.yml');
$excelCache = $excelConfig['cache'];
PHPExcel_Settings::setCacheStorageMethod($excelCache['method'], $excelCache['settings']);

$originsExists = !empty($_SERVER['HTTP_ORIGIN']);
if ($originsExists) {
  $app->register(new CorsServiceProvider(), [
    "cors.allowOrigin"      => $_SERVER['HTTP_ORIGIN'],
    "cors.allowCredentials" => true,
  ]);
}

require_once __DIR__ . '/app.php';

$app->boot();

if ($originsExists) {
  $app->after($app["cors"]);
}

$app->run();

function loadConfigWithCache($path) {
  if (APCU_ENABLED) {
    if (apcu_exists($path)) {
      $result = apcu_fetch($path);
    } else {
      $result = Yaml::parse(file_get_contents($path));
      apcu_add($path, $result);
    }
  } else {
    $result = Yaml::parse(file_get_contents($path));
  }

  return $result;
}

function appInstalled($appConfigFile): bool {
  return APCU_ENABLED && apcu_exists($appConfigFile) || file_exists($appConfigFile);
}
