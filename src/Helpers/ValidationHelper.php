<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers;

class ValidationHelper {

  const NUMBER = "[-|+]?[\d, ]+(\.[\d, ]*)?";
  const DATE_YYYY_MM_DD = "\d{2}\d{0,2}[.,\- \/]((0?[1-9])|(1[0-2]))[.,\- \/]((3[0-1])|([1-2]\d)|(0?[1-9]))";
  const DATE_DD_MM_YYYY = "((3[0-1])|([1-2]\d)|(0?[1-9]))[.,\- \/]((0?[1-9])|(1[0-2]))[.,\- \/]\d{2}\d{0,2}";
  const TIME = "([0-1]?\d|2[0-3])?:[0-5]?\d(:[0-5]?\d){0,2}";
  const DATETIME = self::DATE_YYYY_MM_DD . " " . self::TIME;

  /**
   * @param string|null $src
   * @return bool
   */
  public static function isNumber(string $src): bool {
    return preg_match(sprintf("/^%s$/", self::NUMBER), self::formatNumber($src));
  }

  /**
   * @param string|null $src
   * @return bool
   */
  public static function isDate(string $src): bool {
    return !empty($src) && preg_match(sprintf("/^%s$/", self::DATE_DD_MM_YYYY), $src);
  }

  /**
   * @param string|null $src
   * @return bool
   */
  public static function isTime(string $src): bool {
    return !empty($src) && preg_match(sprintf("/^%s$/", self::TIME), $src);
  }

  public static function formatNumber(string $src) {
    return preg_replace("/(^[,.]+)|(?![-+])(?![,.])\D+/", "", $src);
  }
}

class NumberValidationException extends \Exception {
}

class DateValidationException extends \Exception {
}

class TimeValidationException extends \Exception {
}

class OperationTypeValidationException extends \Exception {
}
