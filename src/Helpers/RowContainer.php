<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers;

use App\Entities\Sale\Check\Full;
use App\Entities\Sale\Check\FullSaleItem;
use App\Entities\Sale\Check\Type;
use DateTime;
use JsonSerializable;

class RowContainer implements JsonSerializable {

  /**
   * @var DateTime
   */
  private $date;
  /**
   * @var int
   */
  private $number;
  /**
   * @var int
   */
  private $till;
  /**
   * @var string
   */
  private $type;
  /**
   * @var float
   */
  private $sum;
  /**
   * @var int
   */
  private $position;
  /**
   * @var string
   */
  private $article;
  /**
   * @var string
   */
  private $name;
  /**
   * @var float
   */
  private $price;
  /**
   * @var int
   */
  private $count;
  /**
   * @var float
   */
  private $discount;
  /**
   * @var bool
   */
  private $isShort = true;

  /**
   * RowContainer constructor.
   * @param DateTime $date
   * @param int $number
   * @param int $till
   * @param string $type
   * @param float $sum
   * @param int $position
   * @param string $article
   * @param string $name
   * @param float $price
   * @param int $count
   * @param float $discount
   * @param bool $isShort
   */
  public function __construct(DateTime $date, int $number, int $till, string $type, float $sum, int $position,
                              string $article, string $name, float $price, int $count, float $discount, bool $isShort) {
    $this->date = $date;
    $this->number = $number;
    $this->till = $till;
    $this->type = $type;
    $this->sum = $sum;
    $this->position = $position;
    $this->article = $article;
    $this->name = $name;
    $this->price = $price;
    $this->count = $count;
    $this->discount = $discount;
    $this->isShort = $isShort;
  }

  /**
   * @return DateTime
   */
  public function getDate(): DateTime {
    return $this->date;
  }

  /**
   * @return int
   */
  public function getNumber(): int {
    return $this->number;
  }

  /**
   * @return int
   */
  public function getTill(): int {
    return $this->till;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * @return float
   */
  public function getSum(): float {
    return $this->sum;
  }

  /**
   * @return int
   */
  public function getPosition(): int {
    return $this->position;
  }

  /**
   * @return string
   */
  public function getArticle(): string {
    return $this->article;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @return float
   */
  public function getPrice(): float {
    return $this->price;
  }

  /**
   * @return int
   */
  public function getCount(): int {
    return $this->count;
  }

  /**
   * @return float
   */
  public function getDiscount(): float {
    return $this->discount;
  }

  /**
   * @return bool
   */
  public function isShort(): bool {
    return $this->isShort;
  }

  public function getSale(): Full {
    $sale = new Full();
    $sale->setType(Type::FULL);

    $sale->setDate($this->getDate());
    $sale->setNumber($this->getNumber());
    $sale->setTill($this->getTill());
    $sale->setOperationType($this->getType());

    return $sale;
  }

  public function getSaleItem(): FullSaleItem {
    $saleItem = new FullSaleItem();

    $saleItem->setArticle($this->getArticle());
    $saleItem->setName($this->getName());
    $saleItem->setPrice(abs($this->getPrice()));
    $saleItem->setAmount(abs($this->getCount()));
    $saleItem->setDiscount(abs($this->getDiscount()));

    return $saleItem;
  }

  public function isValid(): bool {
    return !empty($this->getType())
      && !empty($this->getTill())
      && !empty($this->getNumber())
      && !empty($this->getSum())
      && ($this->getType() == Type::FULL && $this->getSaleItem()->isValid() || $this->getType() != Type::FULL);
  }

  /**
   * Specify data which should be serialized to JSON
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize() {
    return [
      'date'     => DateHelper::formatWithDateTime($this->date),
      'number'   => $this->number,
      'till'     => $this->till,
      'type'     => $this->type,
      'sum'      => $this->sum,
      'position' => $this->position,
      'article'  => $this->article,
      'name'     => $this->name,
      'price'    => $this->price,
      'count'    => $this->count,
      'discount' => $this->discount,
    ];
  }

  /**
   * @param array $obj
   * @return RowContainer
   */
  public static function fromObject($obj): RowContainer {
    $date = $obj[0];
    $time = $obj[1];

    $dateTime = DateHelper::datePlusTime($date, $time);

    $number = self::stringToNumber($obj[2]);

    $till = self::stringToNumber($obj[3]);

    $type = mb_strtolower(trim($obj[4])) == "продажа" ? "SALE" : "RETURN";

    $sum = abs(self::stringToNumber($obj[5]));

    $isShort = true;

    $position = 1;
    if (!empty($obj[6])) {
      $position = self::stringToNumber($obj[6]);
      $isShort = false;
    }

    $article = '-';
    if (!empty($obj[7])) {
      $article = $obj[7];
      $isShort = false;
    }

    $name = '-';
    if (!empty($obj[8])) {
      $name = $obj[8];
      $isShort = false;
    }

    $price = $sum;
    if (!empty($obj[9])) {
      $price = abs(self::stringToNumber($obj[9]));
      $isShort = false;
    }

    $count = 1;
    if (!empty($obj[10])) {
      $count = abs(self::stringToNumber($obj[10]));
      $isShort = false;
    }

    $discount = $price * $count;
    if (!empty($obj[11])) {
      $discount = abs(self::stringToNumber($obj[11]));
      $isShort = false;
    }

    return new RowContainer(
      $dateTime,
      $number,
      $till,
      $type,
      $sum,
      $position,
      $article,
      $name,
      $price,
      $count,
      $discount,
      $isShort
    );
  }

  private static function stringToNumber(string $string): float {
    return floatval(ValidationHelper::formatNumber($string));
  }
}

if ((!function_exists('mb_str_replace')) &&
  (function_exists('mb_substr')) && (function_exists('mb_strlen')) && (function_exists('mb_strpos'))
) {
  function mb_str_replace($search, $replace, $subject) {
    if (is_array($subject)) {
      $ret = [];
      foreach ($subject as $key => $val) {
        $ret[$key] = mb_str_replace($search, $replace, $val);
      }

      return $ret;
    }

    foreach ((array)$search as $key => $s) {
      if ($s == '' && $s !== 0) {
        continue;
      }
      $r = !is_array($replace) ? $replace : (array_key_exists($key, $replace) ? $replace[$key] : '');
      $pos = mb_strpos($subject, $s, 0, 'UTF-8');
      while ($pos !== false) {
        $subject = mb_substr($subject, 0, $pos, 'UTF-8') . $r . mb_substr($subject, $pos + mb_strlen($s, 'UTF-8'), 65535, 'UTF-8');
        $pos = mb_strpos($subject, $s, $pos + mb_strlen($r, 'UTF-8'), 'UTF-8');
      }
    }

    return $subject;
  }
}
