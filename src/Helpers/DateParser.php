<?php
/**
 * Created by IntelliJ IDEA.
 * User: redcreepster
 * Date: 04.07.17
 * Time: 20:09
 */

namespace App\Helpers;


use DateTime;

class DateParser {
  function __invoke(string $dateString): DateTime {
    return new DateTime($dateString);
  }
}
