<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Formatter;

use App\Helpers\Excel\ExcelLoader;
use PHPExcel_Cell;
use PHPExcel_Style;
use PHPExcel_Style_NumberFormat;

class ExcelFormatterImpl extends ExcelLoader implements ExcelFormatter {

  const GENERAL_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_GENERAL;
  const DATE_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY;
  const TIME_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4;
  const NUMBER_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00;

  public function format() {
    $sheet = $this->excel->getActiveSheet();
    for ($rowIndex = 1; $rowIndex < $this->getRowsCount() + 1; $rowIndex++) {
      /**
       * Строки не существует
       */
      if (!$sheet->cellExistsByColumnAndRow(0, $rowIndex)) {
        continue;
      }
      /**
       * Строка пуста
       */
      if (empty($sheet->getCellByColumnAndRow(0, $rowIndex)->getFormattedValue())) {
        continue;
      }

      $columnHighest = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
      if ($columnHighest > self::MAX_COLUMNS_COUNT) {
        $columnHighest = self::MAX_COLUMNS_COUNT;
      }
      $isShort = $columnHighest < self::MAX_COLUMNS_COUNT;
      for ($columnIndex = 0; $columnIndex < $columnHighest; $columnIndex++) {
        $style = $sheet->getStyleByColumnAndRow($columnIndex, $rowIndex);
        if ($isShort) {
          if (!$this->formatShortValid($columnIndex, $style)) {
            $this->formatShort($columnIndex, $style);
          }
        } else {
          if (!$this->formatFullValid($columnIndex, $style)) {
            $this->formatFull($columnIndex, $style);
          }
        }
      }
    }
  }

  private function formatShortValid(int $columnIndex, PHPExcel_Style $style): bool {
    switch ($columnIndex) {
      case 0:
        return $style->getNumberFormat()->getFormatCode() == self::DATE_FORMAT;
      case 1:
        return $style->getNumberFormat()->getFormatCode() == self::TIME_FORMAT;
      case 3:
        return $style->getNumberFormat()->getFormatCode() == self::NUMBER_FORMAT;
      default:
        return $style->getNumberFormat()->getFormatCode() == self::GENERAL_FORMAT;
    }
  }

  private function formatFullValid(int $columnIndex, PHPExcel_Style $style): bool {
    switch ($columnIndex) {
      case 0:
        return $style->getNumberFormat()->getFormatCode() == self::DATE_FORMAT;
      case 1:
        return $style->getNumberFormat()->getFormatCode() == self::TIME_FORMAT;
      case 5:
      case 9:
      case 11:
        return $style->getNumberFormat()->getFormatCode() == self::NUMBER_FORMAT;
      default:
        return $style->getNumberFormat()->getFormatCode() == self::GENERAL_FORMAT;
    }
  }

  private function formatShort(int $columnIndex, PHPExcel_Style $style) {
    switch ($columnIndex) {
      case 0:
        $style->getNumberFormat()->setFormatCode(self::DATE_FORMAT);
        break;
      case 1:
        $style->getNumberFormat()->setFormatCode(self::TIME_FORMAT);
        break;
      case 3:
        $style->getNumberFormat()->setFormatCode(self::NUMBER_FORMAT);
        break;
      default:
        $style->getNumberFormat()->setFormatCode(self::GENERAL_FORMAT);
        break;
    }
  }

  private function formatFull(int $columnIndex, PHPExcel_Style $style) {
    switch ($columnIndex) {
      case 0:
        $style->getNumberFormat()->setFormatCode(self::DATE_FORMAT);
        break;
      case 1:
        $style->getNumberFormat()->setFormatCode(self::TIME_FORMAT);
        break;
      case 5:
      case 9:
      case 11:
        $style->getNumberFormat()->setFormatCode(self::NUMBER_FORMAT);
        break;
      default:
        $style->getNumberFormat()->setFormatCode(self::GENERAL_FORMAT);
        break;
    }
  }

}
