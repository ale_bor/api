<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Cache;

interface Cache {

  /**
   * Инициализирует новое хранилище
   * @param string $id
   */
  public function __construct(string $id);

  /**
   * Возвращает ассоциативный массив из файла.
   * В случае отсутствия данных, возвращает пустой массив.
   * @return array
   */
  public function loadCache(): array;

  /**
   * Сохраняет ассоциативный массив в кэше
   * @param array $data
   */
  public function writeCache(array $data);

  /**
   * Удаляет данные из кэша
   */
  public function cleanCache();

}
