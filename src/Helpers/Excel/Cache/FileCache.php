<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Cache;

class FileCache implements Cache {

  /**
   * @var string Путь для сохранения данных
   */
  private $path;

  /**
   * Инициализирует новое хранилище
   * @param string $id
   * @param string $path Путь до директории
   */
  public function __construct(string $id, string $path = EXCEL_TEMP_DIR) {
    if (!file_exists($path) || !is_dir($path)) {
      mkdir($path, 0777, true);
    }
    $this->path = $path . '/' . $id . '.cache.dat';
  }

  /**
   * Возвращает ассоциативный массив из файла.
   * В случае отсутствия данных, возвращает пустой массив.
   * @return array
   */
  public function loadCache(): array {
    $data = [];
    if (file_exists($this->path)) {
      $tmp = unserialize(file_get_contents($this->path));
      if ($tmp !== false) {
        $data = $tmp;
      }
    }

    return $data;
  }

  /**
   * Сохраняет ассоциативный массив в кэше
   * @param array $data
   */
  public function writeCache(array $data) {
    file_put_contents($this->path, serialize($data));
  }

  /**
   * Удаляет данные из кэша
   */
  public function cleanCache() {
    if (file_exists($this->path)) {
      unlink($this->path);
    }
  }
}
