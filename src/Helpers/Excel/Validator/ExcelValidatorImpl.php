<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Validator;

use App\Helpers\DateValidationException;
use App\Helpers\Excel\Formatter\ExcelFormatterImpl;
use App\Helpers\NumberValidationException;
use App\Helpers\OperationTypeValidationException;
use App\Helpers\TimeValidationException;
use App\Helpers\ValidationHelper;
use Exception;
use PHPExcel_Cell;

class ExcelValidatorImpl extends ExcelFormatterImpl implements ExcelValidator {
  /**
   * @var Exception[]
   */
  private $errors = [];

  public function isValid(): bool {
    if (!empty($this->errors)) {
      return false;
    }
    if (empty($this->excel)) {
      return false;
    }

    $sheet = $this->excel->getActiveSheet();
    //Skip header on first row
    for ($rowIndex = 2; $rowIndex <= $this->getRowsCount() + 1; $rowIndex++) {
      if (!$sheet->cellExistsByColumnAndRow(0, $rowIndex)) {
        continue;
      }
      $cell = $sheet->getCellByColumnAndRow(0, $rowIndex);
      if (empty($cell->getValue())) {
        continue;
      }

      $nColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

      for ($columnIndex = 0; $columnIndex < $nColumn && $columnIndex < self::MAX_COLUMNS_COUNT; $columnIndex++) {
        $cell = $sheet->getCellByColumnAndRow($columnIndex, $rowIndex);

        $this->validateCell($columnIndex, $rowIndex, $cell);
      }
    }

    return empty($this->errors);
  }

  private function validateCell(int $columnIndex, int $rowIndex, PHPExcel_Cell $cell) {
    if (!empty($cell->getFormattedValue())) {
      switch ($columnIndex) {
        //Дата
        case 0:
          if (!self::isDate($cell)) {
            $this->errors[] = new DateValidationException("Неверный формат даты в строке $rowIndex");
          }
          break;
        //Время
        case 1:
          if (!self::isTime($cell)) {
            $this->errors[] = new TimeValidationException("Неверный формат времени в строке $rowIndex");
          }
          break;
        //Тип операции
        case 4:
          $value = mb_strtolower(trim($cell->getValue()));
          if ($value !== "продажа" && $value !== "возврат") {
            $this->errors[] = new OperationTypeValidationException("Неверный тип операции в строке $rowIndex");
          }
          break;
        //Номер чека
        case 5:
        case 9:
        case 10:
        case 11:
          if (!ValidationHelper::isNumber($cell->getValue())) {
            $this->errors[] = new NumberValidationException("Неверный формат числа в строке $rowIndex");
          }
          break;
      }
    }
  }

  private function isDate(PHPExcel_Cell $cell): bool {
    return ValidationHelper::isDate($cell->getFormattedValue());
  }

  private function isTime(PHPExcel_Cell $cell): bool {
    return ValidationHelper::isTime($cell->getFormattedValue());
  }

  /**
   * @return Exception[]
   */
  public function errors(): array {
    if (empty($this->excel)) {
      return [new Exception("Не удалось обработать файл")];
    } else {
      return $this->errors;
    }
  }
}
