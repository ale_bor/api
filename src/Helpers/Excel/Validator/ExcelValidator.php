<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Validator;

use App\Helpers\Excel\Formatter\ExcelFormatter;
use Exception;

interface ExcelValidator extends ExcelFormatter {

  public function isValid(): bool;

  /**
   * @return Exception[]
   */
  public function errors(): array;

}
