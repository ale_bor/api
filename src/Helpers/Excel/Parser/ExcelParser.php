<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Parser;

use App\Helpers\Excel\Cache\Cache;
use App\Helpers\Excel\Meta;
use App\Helpers\Excel\Validator\ExcelValidator;

/**
 * Пакетная обработка данных
 * @package App\Helpers\Excel\Parser
 */
interface ExcelParser extends ExcelValidator, Meta {

  public function setCache(Cache $cache);

  public function getTurnSize(): int;

  /**
   * @return int[]
   */
  public function splitByDate(): array;

  public function getPosition(): int;

  public function setPosition(int $position);

  public function runNext();

  public function parse(int $startRow, int $endRow);

  public function setPointId(int $pointId);

  public function getPointId(): int;

  public function getData(): array;

  public function purge();

}
