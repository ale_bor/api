<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel\Parser;

use App\Helpers\Excel\Cache\Cache;
use App\Helpers\Excel\Cache\FileCache;
use App\Helpers\Excel\ExcelLoader;
use App\Helpers\Excel\FileNotFoundException;
use App\Helpers\Excel\Validator\ExcelValidatorImpl;
use App\Helpers\ValidationHelper;
use DateTime;
use PHPExcel_Cell;

class ExcelParserImpl extends ExcelValidatorImpl implements ExcelParser {

  const HEADER_OFFSET = 1;
  const MAX_TURN_STEP_SIZE = 3000;

  /**
   * @var int[]
   */
  private $splitByDate = [];
  /**
   * @var int
   */
  private $position = 0;
  /**
   * @var Cache
   */
  private $cache = null;
  /**
   * @var array
   */
  private $data = [];
  /**
   * @var int
   */
  private $pointId = 0;

  /**
   * ExcelParserImpl constructor.
   * @param string $filePath Путь к файлу
   * @param string $hash Первоначальный sha1 хэш файла
   * @param string $session Id сессии
   * @param bool $format
   * @param string $csvEncoding Кодировка CSV файла
   * @throws FileNotFoundException
   */
  public function __construct(string $filePath, string $hash = '', string $session = '', bool $format = false, string $csvEncoding = 'windows-1251') {
    parent::__construct($filePath, $hash, $session, $csvEncoding);

    if ($format) {
      $this->format();
    }

    if ($this->isSaved()) {
      $this->loadMeta();
    }

    $this->setCache(new FileCache($this->hash() . $this->getSession()));
  }

  public function setCache(Cache $cache) {
    $this->cache = $cache;
    if ($this->isSaved()) {
      $this->data = $cache->loadCache();
    }
  }

  public function getTurnSize(): int {
    return count($this->splitByDate());
  }

  /**
   * @return int[]
   */
  public function splitByDate(): array {
    if (empty($this->splitByDate)) {

      $this->splitByDate = [];

      $sheet = $this->excel->getActiveSheet();
      /**
       * @var $lastDate DateTime
       */
      $lastDate = null;
      $rowIndex = 1;
      for (; $rowIndex < $sheet->getHighestRow() + 1; $rowIndex++) {
        if (!$sheet->cellExistsByColumnAndRow(0, $rowIndex + self::HEADER_OFFSET)) {
          continue;
        }
        $cell = $sheet->getCellByColumnAndRow(0, $rowIndex + self::HEADER_OFFSET);
        /**
         * @var $date DateTime
         */
        $date = $this->parseDate($cell);
        if (empty($lastDate)) {
          $lastDate = $date;
          $this->splitByDate[] = $rowIndex;
        } else if ($lastDate->getTimestamp() != $date->getTimestamp()) {
          $count = count($this->splitByDate);
          $index = $this->splitByDate[$count - 1];
          if ($rowIndex - $index > self::MAX_TURN_STEP_SIZE) {
            $this->splitByDate[] = $rowIndex;
          }
          $lastDate = $date;
        }
      }
      $this->splitByDate[] = $rowIndex + 1;
    }

    return $this->splitByDate;
  }

  public function getPosition(): int {
    return $this->position;
  }

  public function setPosition(int $position) {
    $this->position = $position;
  }

  public function runNext() {
    $splitByDate = $this->splitByDate();
    $startRow = $splitByDate[$this->getPosition()];
    $endRow = $startRow;
    if (count($splitByDate) - 1 > $this->getPosition()) {
      $endRow = $splitByDate[$this->getPosition() + 1];
    }

    $this->parse($startRow, $endRow);
    $this->cache->writeCache($this->data);
    $this->setPosition($this->getPosition() + 1);
    $this->writeMeta();

    return $this->data;
  }

  public function parse(int $startRow, int $endRow) {
    $sheet = $this->excel->getActiveSheet();
    for ($rowIndex = $startRow + 1; $rowIndex <= $endRow; $rowIndex++) {
      if (!$sheet->cellExistsByColumnAndRow(0, $rowIndex)) {
        continue;
      }
      $cell = $sheet->getCellByColumnAndRow(0, $rowIndex);
      if (empty($cell->getValue())) {
        continue;
      }
      $row = [];

      $nColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

      for ($columnIndex = 0; $columnIndex < $nColumn && $columnIndex < self::MAX_COLUMNS_COUNT; $columnIndex++) {
        $cell = $sheet->getCellByColumnAndRow($columnIndex, $rowIndex);

        if (empty($cell->getFormattedValue()) && $columnIndex > 11) {
          break;
        }

        if (self::isDate($cell)) {
          $value = self::parseDate($cell);
        } else if (self::isTime($cell)) {
          $value = self::parseTime($cell);
        } else {
          $value = trim($cell->getValue());
        }
        unset($cell);

        if (empty($value) && !is_numeric($value)) {
          $value = "";
        }

        $row[$columnIndex] = $value;
        unset($value);
      }
      unset($nColumn);

      $this->data[] = $row;
      unset($row);
    }
  }

  public function setPointId(int $pointId) {
    $this->pointId = $pointId;
  }

  public function getPointId(): int {
    return $this->pointId;
  }

  private function isDate(PHPExcel_Cell $cell): bool {
    return ValidationHelper::isDate($cell->getFormattedValue());
  }

  private function isTime(PHPExcel_Cell $cell): bool {
    return ValidationHelper::isTime($cell->getFormattedValue());
  }

  private function parseDate(PHPExcel_Cell $cell, bool $validateFormat = false): DateTime {
    if ($validateFormat) {
      if ($cell->getStyle()->getNumberFormat()->getFormatCode() != self::DATE_FORMAT) {
        $cell->getStyle()->getNumberFormat()->setFormatCode(self::DATE_FORMAT);
      }
    }
    $date = new DateTime('00:00:00');

    $ddd = preg_split("/[.,\- \/]/", $cell->getFormattedValue());

    if (count($ddd) == 3) {
      $day = $ddd[0];
      $month = $ddd[1];
      $year = $ddd[2];
      if (strlen($year) == 2) {
        $year = "20" . $year;
      }
      $date->setDate($year, $month, $day);
    }

    return $date;
  }

  private function parseTime(PHPExcel_Cell $cell, bool $validateFormat = false): DateTime {
    if ($validateFormat) {
      if ($cell->getStyle()->getNumberFormat()->getFormatCode() != self::TIME_FORMAT) {
        $cell->getStyle()->getNumberFormat()->setFormatCode(self::TIME_FORMAT);
      }
    }
    $date = new DateTime('1970-1-1');

    $ddd = explode(":", $cell->getFormattedValue());

    if (count($ddd) >= 2) {
      $hour = $ddd[0];
      $minutes = $ddd[1];
      $seconds = count($ddd) >= 3 ? $ddd[2] : 0;
      $date->setTime($hour, $minutes, $seconds);
    }

    return $date;
  }

  public function getData(): array {
    return $this->data;
  }

  public function purge() {
    $this->cache->cleanCache();
    $this->removeMeta();
    $this->removeFile();
  }

  public function loadMeta() {
    $meta = [];
    $metaPath = $this->metaPath();
    if (file_exists($metaPath)) {
      $tmp = unserialize(file_get_contents($metaPath));
      if ($tmp !== false) {
        $meta = $tmp;
      }
    }

    $this->metaLoaded($meta);
  }

  public function writeMeta() {
    $metaPath = $this->metaPath();
    $dir = dirname($metaPath);
    if (!file_exists($dir) || !is_dir($dir)) {
      mkdir($dir, 0777, true);
    }
    file_put_contents($metaPath, serialize($this->meta()));
  }

  public function metaLoaded(array $meta) {
    if (isset($meta['turn'])) {
      $this->splitByDate = $meta['turn'];
    }
    $turnPosition = 0;
    if (isset($meta['turnPosition'])) {
      $turnPosition = $meta['turnPosition'];
    }
    $this->setPosition($turnPosition);

    $pointId = 0;
    if (isset($meta['pointId'])) {
      $pointId = $meta['pointId'];
    }
    $this->setPointId($pointId);
  }

  public function meta(): array {
    return [
      'turn'         => $this->splitByDate(),
      'turnPosition' => $this->getPosition(),
      'pointId'      => $this->getPointId(),
    ];
  }

  public function removeMeta() {
    if (file_exists($this->metaPath())) {
      unlink($this->metaPath());
    }
  }

  /**
   * @param string $hash Первоначальный sha1 хэш файла
   * @param string $session Id сессии
   * @return ExcelParser
   * @throws FileNotFoundException
   */
  public static function continue(string $hash, string $session): ExcelParser {
    $filePath = ExcelLoader::filePathFromHash($hash, $session);
    $format = false;
    $csvEncoding = 'UTF-8';

    return new ExcelParserImpl($filePath, $hash, $session, $format, $csvEncoding);
  }
}
