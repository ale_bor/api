<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel;


interface Meta {

  public function loadMeta();

  public function writeMeta();

  public function metaLoaded(array $meta);

  public function meta(): array;

  public function removeMeta();

}
