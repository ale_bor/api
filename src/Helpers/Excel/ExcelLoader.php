<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel;

use Exception;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_Reader_CSV;
use PHPExcel_Writer_CSV;

abstract class ExcelLoader {
  const MAX_COLUMNS_COUNT = 12;
  /**
   * @var string
   */
  private $filePath;
  /**
   * @var \PHPExcel_Reader_IReader
   */
  private $reader;
  /**
   * @var PHPExcel
   */
  protected $excel;
  /**
   * @var int
   */
  private $rowsCount = 0;
  /**
   * @var string
   */
  private $hash;
  /**
   * @var string
   */
  private $session;

  /**
   * @param string $filePath Путь к файлу
   * @param string $hash Первоначальный sha1 хэш файла
   * @param string $session Id сессии
   * @param string $csvEncoding Кодировка CSV файла
   * @throws FileNotFoundException
   */
  public function __construct(string $filePath, string $hash = '', string $session = '', string $csvEncoding = 'windows-1251') {
    $this->init($filePath, $hash, $session, $csvEncoding);
  }

  /**
   * @param string $filePath Путь к файлу
   * @param string $hash Первоначальный sha1 хэш файла
   * @param string $session Id сессии
   * @param string $csvEncoding Кодировка CSV файла
   * @throws FileNotFoundException
   */
  private function init(string $filePath, string $hash, string $session, string $csvEncoding) {
    $this->filePath = $filePath;
    $this->hash = $hash;
    $this->session = $session;

    if (!empty($filePath)) {
      $readerOrWriterType = PHPExcel_IOFactory::identify($filePath);
      $this->reader = PHPExcel_IOFactory::createReader($readerOrWriterType);

      if ($readerOrWriterType == "CSV" || $this->reader instanceof PHPExcel_Reader_CSV) {
        /**
         * @var $reader PHPExcel_Reader_CSV
         */
        $reader = $this->reader;
        $reader->setDelimiter($this->detectCSVDelimiter($filePath));
        $reader->setInputEncoding($csvEncoding);
      }

      $this->excel = $this->reader->load($filePath);
    } else {
      throw new FileNotFoundException("Файл \"$filePath\" не найден");
    }
  }

  private function detectCSVDelimiter(string $file): string {
    $delimiters = [
      ';'  => 0,
      ','  => 0,
      "\t" => 0,
      "|"  => 0,
      ":"  => 0,
    ];

    $handle = fopen($file, "r");
    $firstLine = fgets($handle);
    fclose($handle);
    foreach ($delimiters as $delimiter => &$count) {
      $count = count(str_getcsv($firstLine, $delimiter));
    }

    return array_search(max($delimiters), $delimiters);
  }

  public function hash(): string {
    if (empty($this->hash)) {
      $this->hash = sha1_file($this->filePath);
    }

    return $this->hash;
  }

  public function getSession(): string {
    return $this->session;
  }

  public function setSession(string $session) {
    $this->session = $session;
  }

  public function path(): string {
    return self::filePathFromHash($this->hash(), $this->session);
  }

  public function metaPath(): string {
    return EXCEL_TEMP_DIR . '/' . $this->hash() . $this->getSession() . '.meta.dat';
  }

  public function getRowsCount(): int {
    if ($this->rowsCount == 0) {
      $sheet = $this->excel->getActiveSheet();
      for ($rowIndex = 1; $rowIndex < $sheet->getHighestRow() + 1; $rowIndex++) {
        if (!$sheet->cellExistsByColumnAndRow(0, $rowIndex)) {
          continue;
        }
        $this->rowsCount++;
      }
    }

    return $this->rowsCount;
  }

  public function isShort(): bool {
    $columnHighest = PHPExcel_Cell::columnIndexFromString($this->excel->getActiveSheet()->getHighestColumn());
    if ($columnHighest > self::MAX_COLUMNS_COUNT) {
      $columnHighest = self::MAX_COLUMNS_COUNT;
    }
    $isShort = $columnHighest < self::MAX_COLUMNS_COUNT;

    return $isShort;
  }

  public function isSaved(): bool {
    $basename = basename($this->filePath);

    return substr($basename, 0, strpos($basename, '.')) == ($this->hash() . $this->session);
  }

  public function isEmpty(): bool {
    return $this->getRowsCount() <= 1;
  }

  /**
   * Перемещает файл
   */
  public function copyFile() {
    if (!file_exists(EXCEL_TEMP_DIR) || !is_dir(EXCEL_TEMP_DIR)) {
      mkdir(EXCEL_TEMP_DIR, 0777, true);
    }

    /**
     * @var $objWriter PHPExcel_Writer_CSV
     */
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->setUseBOM(true);
    $objWriter->save($this->path());

    $this->init($this->path(), $this->hash(), $this->getSession(), 'UTF-8');
  }

  public function removeFile() {
    if (file_exists($this->path())) {
      unlink($this->path());
    }
  }

  /**
   * @param $file
   * @return bool
   */
  public static function isSupportedExcelFormat($file): bool {
    $name = $file['name'];
    $type = $file['type'];

    $mimes = [
      'text/csv',
      'text/plain',
      'application/csv',
      'text/comma-separated-values',
      'application/excel',
      'application/vnd.ms-excel',
      'application/vnd.msexcel',
      'text/anytext',
      'application/octet-stream',
      'application/txt',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/vnd.ms-word.document.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
      'application/vnd.ms-word.template.macroEnabled.12',
      'application/vnd.ms-excel.sheet.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
      'application/vnd.ms-excel.template.macroEnabled.12',
      'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
      'application/vnd.ms-excel.addin.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
      'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.presentationml.template',
      'application/vnd.ms-powerpoint.template.macroEnabled.12',
      'application/vnd.ms-powerpoint.addin.macroEnabled.12',
      'application/vnd.openxmlformats-officedocument.presentationml.slide',
      'application/vnd.ms-powerpoint.slide.macroEnabled.12',
      'application/msonenote',
      'application/vnd.ms-officetheme',
    ];

    return in_array($type, $mimes) && preg_match("/^.+\.(csv|xlsx|xls)$/", $name);
  }

  public static function filePathFromHash(string $hash, string $session): string {
    return EXCEL_TEMP_DIR . '/' . $hash . $session . '.tmp';
  }

}

class InvalidExcelFormatException extends Exception {
}

class FileNotFoundException extends Exception {
}
