<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel;

use App\Entities\Sale\Check\Full;
use App\Entities\Sale\Check\FullSaleItem;
use App\Entities\Sale\Check\Type;
use App\Helpers\DateHelper;
use Exception;
use PHPExcel;
use PHPExcel_CachedObjectStorageFactory;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_Settings;
use PHPExcel_Shared_Date;
use PHPExcel_Style;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;

class ExcelWriter {
  const MAX_COLUMNS_COUNT = 12;
  /**
   * @var string
   */
  private $writerType;
  /**
   * @var bool
   */
  private $isShort;
  /**
   * @var bool
   */
  private $printHtml;
  /**
   * @var bool
   */
  private $onlyTotals;
  /**
   * @var PHPExcel
   */
  private $excel;

  const HEADER_SHORT = [
    'Дата',
    'Сумма продаж',
    'Количество продаж',
    'Сумма возвратов',
    'Количество возвратов',
    'Итого',
  ];

  const HEADER_FULL = [
    'Дата чека',
    'Время чека',
    'Номер чека',
    'Номер кассы',
    'Тип операции',
    'Сумма чека',
    'Номер позиции',
    'Артикул',
    'Наименование',
    'Цена',
    'Количество',
    'Сумма позиции со скидкой',
  ];

  const GENERAL_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_GENERAL;
  const DATE_FORMAT = 'dd.mm.yyyy';
  const TIME_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME3;
  const NUMBER_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_NUMBER;
  const PRICE_FORMAT = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00;

  /**
   * ExcelHelper constructor.
   * @param string $writerType
   * @param bool $isShort
   * @param bool $onlyTotals
   * @param bool $printHtml
   */
  private function __construct(string $writerType, bool $isShort, bool $onlyTotals = false, bool $printHtml = true) {
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    $cacheSettings = ['memoryCacheSize' => '256MB'];
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

    $writerType = strtoupper($writerType);
    switch ($writerType) {
      case 'XLS':
        $this->writerType = 'Excel5';
        break;
      case 'XLSX':
        $this->writerType = 'Excel2007';
        break;
      case 'HTML':
        $this->writerType = 'PRINT_HTML';
        break;
      default:
        $this->writerType = $writerType;
        break;
    }

    $this->printHtml = $printHtml;
    $this->onlyTotals = $onlyTotals;
    $this->isShort = $isShort;

    $this->excel = new PHPExcel();
    $this->excel->setActiveSheetIndex(0);
  }

  private function writeSheetName(string $startDate, string $endDate, string $type) {
    $date = $startDate . ' - ' . $endDate;
    $this->excel->getProperties()->setTitle("$type $date");
    //Оставлю след в истории
    $this->excel->getProperties()->setCreator(base64_decode('aHR0cDovL3JlZHByb2plY3RzLnRr'));
  }

  /**
   * @param int $columnIndexStart
   * @param int $rowIndexStart
   * @param int $columnIndexEnd
   * @param int $rowIndexEnd
   * @param string $formatCode
   * @param bool $bold
   * @param string|null $horizontalAlignment
   * @param string|null $verticalAlignment
   * @param bool $forceFormatCode
   */
  private function formatRange(int $columnIndexStart, int $rowIndexStart, int $columnIndexEnd, int $rowIndexEnd, string $formatCode, bool $bold = false, string $horizontalAlignment = null, string $verticalAlignment = null) {
    $style = $this->excel->getActiveSheet()->getStyleByColumnAndRow($columnIndexStart, $rowIndexStart, $columnIndexEnd, $rowIndexEnd);
    $this->formatStyle($style, $formatCode, $bold, $horizontalAlignment, $verticalAlignment);
  }

  /**
   * @param int $columnIndex
   * @param int $rowIndex
   * @param string $formatCode
   * @param bool $bold
   * @param string|null $horizontalAlignment
   * @param string|null $verticalAlignment
   */
  private function format(int $columnIndex, int $rowIndex, string $formatCode, bool $bold = false, string $horizontalAlignment = null, string $verticalAlignment = null) {
    $style = $this->excel->getActiveSheet()->getStyleByColumnAndRow($columnIndex, $rowIndex);
    $this->formatStyle($style, $formatCode, $bold, $horizontalAlignment, $verticalAlignment);
  }

  /**
   * @param PHPExcel_Style $style
   * @param string $formatCode
   * @param bool $bold
   * @param string|null $horizontalAlignment
   * @param string|null $verticalAlignment
   * @param bool $forceFormatCode
   */
  private function formatStyle(PHPExcel_Style $style, string $formatCode, bool $bold = false, string $horizontalAlignment = null, string $verticalAlignment = null, bool $forceFormatCode = false) {
    $numberFormat = $style->getNumberFormat();
    if ($numberFormat->getFormatCode() !== $formatCode || $forceFormatCode) {
      $numberFormat->setFormatCode($formatCode);
    }
    if ($this->writerType !== 'CSV') {
      $font = $style->getFont();
      if ($font->getBold() !== $bold) {
        $font->setBold($bold);
      }
      if (!empty($horizontalAlignment)) {
        $style->getAlignment()->setHorizontal($horizontalAlignment);
      }
      if (!empty($verticalAlignment)) {
        $style->getAlignment()->setVertical($verticalAlignment);
      }
    }
  }

  /**
   * @param int $rowIndex
   */
  public function writeHeader(int $rowIndex = 1) {
    $header = $this->isShort ? self::HEADER_SHORT : self::HEADER_FULL;
    $headerSize = count($header);

    $sheet = $this->excel->getActiveSheet();
    for ($i = 0; $i < $headerSize; $i++) {
      $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
      $sheet->setCellValueByColumnAndRow($i, $rowIndex, $header[$i]);
    }
    $horizontalAlignment = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
    $this->formatRange(0, $rowIndex, $headerSize - 1, $rowIndex, self::GENERAL_FORMAT, true, $horizontalAlignment);

    $sheet->freezePaneByColumnAndRow($headerSize - 1, $rowIndex + 1);
  }

  /**
   * @param Full[] $sales
   * @param int $startRowIndex
   */
  public function writeSales(array $sales, int $startRowIndex = 2) {
    if (!$this->onlyTotals) {
      if ($this->isShort) {
        $this->writeShortSales($sales, $startRowIndex);
      } else {
        $this->writeFullSales($sales, $startRowIndex);
      }
    }
  }

  /**
   * @param Full[] $sales
   * @param int $startRowIndex
   */
  private function writeShortSales(array $sales, int $startRowIndex = 2) {
    $sheet = $this->excel->getActiveSheet();
    $rowIndex = $startRowIndex;
    $byDates = [];

    foreach ($sales as $sale) {
      $ts = DateHelper::dateWithoutTime($sale->getDate())->getTimestamp();
      if (!array_key_exists($ts, $byDates)) {
        $byDates[$ts] = [];
      }

      $byDates[$ts][] = $sale;
    }

    foreach ($byDates as $byDate) {
      $salesCount = 0;
      $salesSum = 0;
      $returnCount = 0;
      $returnSum = 0;
      /**
       * @var $sale Full
       */
      foreach ($byDate as $sale) {
        if ($sale->getOperationType() == 'SALE') {
          $salesCount++;
          $salesSum += $sale->getSum();
        } else {
          $returnCount++;
          $returnSum += $sale->getSum();
        }
      }

      //Дата
      $cell = $sheet->setCellValueByColumnAndRow(0, $rowIndex, PHPExcel_Shared_Date::PHPToExcel($sale->getDate()), true);
      $this->formatStyle($cell->getStyle(), self::DATE_FORMAT);

      //Сумма продаж за день
      $cell = $sheet->setCellValueByColumnAndRow(1, $rowIndex, $salesSum, true);
      $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

      //Количество продаж за день
      $cell = $sheet->setCellValueByColumnAndRow(2, $rowIndex, $salesCount, true);
      $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

      //Сумма возвратов за день
      $cell = $sheet->setCellValueByColumnAndRow(3, $rowIndex, '-' . abs($returnSum), true);
      $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

      //Кличество возвратов за день
      $cell = $sheet->setCellValueByColumnAndRow(4, $rowIndex, $returnCount, true);
      $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

      //Итого за день
      $cell = $sheet->setCellValueByColumnAndRow(5, $rowIndex, $salesSum - abs($returnSum), true);
      $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

      $rowIndex++;
    }
  }

  /**
   * @param Full[] $sales
   * @param int $startRowIndex
   */
  private function writeFullSales(array $sales, int $startRowIndex = 2) {
    $sheet = $this->excel->getActiveSheet();
    $rowIndex = $startRowIndex;
    for ($i = 0; $i < count($sales); $i++) {
      $sale = $sales[$i];
      if (empty($sale) || empty($sale->getItems())) {
        continue;
      }
      for ($j = 0; $j < count($sale->getItems()); $j++) {
        $isSale = $sale->getOperationType() == 'SALE';

        //Дата чека
        $cell = $sheet->setCellValueByColumnAndRow(0, $rowIndex, PHPExcel_Shared_Date::PHPToExcel($sale->getDate()), true);
        $this->formatStyle($cell->getStyle(), self::DATE_FORMAT);

        //Время чека
        $cell = $sheet->setCellValueByColumnAndRow(1, $rowIndex, PHPExcel_Shared_Date::PHPToExcel($sale->getDate()), true);
        $this->formatStyle($cell->getStyle(), self::TIME_FORMAT);

        //Номер чека
        $cell = $sheet->setCellValueByColumnAndRow(2, $rowIndex, $sale->getNumber(), true);
        $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

        //Номер кассы
        $cell = $sheet->setCellValueByColumnAndRow(3, $rowIndex, $sale->getTill(), true);
        $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

        //Тип операции
        $saleType = $isSale ? 'Продажа' : 'Возврат';
        $cell = $sheet->setCellValueByColumnAndRow(4, $rowIndex, $saleType, true);
        $this->formatStyle($cell->getStyle(), self::GENERAL_FORMAT);

        //Сумма чека
        $textSum = $isSale ? $sale->getSum() : ('-' . abs($sale->getSum()));
        $cell = $sheet->setCellValueByColumnAndRow(5, $rowIndex, $textSum, true);
        $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

        /**
         * @var FullSaleItem $item
         */
        $item = $sale->getItems()[$j];

        //Номер позиции
        $cell = $sheet->setCellValueByColumnAndRow(6, $rowIndex, $j + 1, true);
        $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

        //Артикул
        $cell = $sheet->setCellValueByColumnAndRow(7, $rowIndex, $item->getArticle(), true);
        $this->formatStyle($cell->getStyle(), self::GENERAL_FORMAT);

        //Наименование
        $cell = $sheet->setCellValueByColumnAndRow(8, $rowIndex, $item->getName(), true);
        $this->formatStyle($cell->getStyle(), self::GENERAL_FORMAT);

        //Цена
        $textPrice = $isSale ? $item->getPrice() : ('-' . abs($item->getPrice()));
        $cell = $sheet->setCellValueByColumnAndRow(9, $rowIndex, $textPrice, true);
        $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

        //Количество
        $textAmount = $isSale ? $item->getAmount() : ('-' . abs($item->getAmount()));
        $cell = $sheet->setCellValueByColumnAndRow(10, $rowIndex, $textAmount, true);
        $this->formatStyle($cell->getStyle(), self::NUMBER_FORMAT);

        //Сумма позиции со скидкой
        $discount = $item->getDiscount() > 0 ? $item->getDiscount() : $item->getPrice() * $item->getAmount();
        $textDiscount = $isSale ? $discount : ('-' . abs($discount));
        $cell = $sheet->setCellValueByColumnAndRow(11, $rowIndex, $textDiscount, true);
        $this->formatStyle($cell->getStyle(), self::PRICE_FORMAT);

        $rowIndex++;
      }
    }
  }

  /**
   * @param Full[] $sales
   */
  public function writeFooter(array $sales) {
    if ($this->isShort) {
      $this->writeShortFooter($sales);
    } else {
      $this->writeFullFooter($sales);
    }
  }

  /**
   * @param Full[] $sales
   */
  private function writeShortFooter(array $sales) {
    $saleCount = 0;
    $saleSum = 0;
    $returnCount = 0;
    $returnSum = 0;

    $rowsCount = 0;
    $lastDate = null;

    foreach ($sales as $sale) {
      $dwf = DateHelper::dateWithoutTime($sale->getDate());
      if ($lastDate != $dwf) {
        $rowsCount++;
        $lastDate = $dwf;
      }
      if ($sale->getOperationType() == "SALE") {
        $saleCount++;
        $saleSum += $sale->getSum();
      } else {
        $returnCount++;
        $returnSum += $sale->getSum();
      }
    }

    $rowIndex = 2;
    if (!$this->onlyTotals) {
      $rowIndex += $rowsCount;
    }

    $sheet = $this->excel->getActiveSheet();

    $sheet->setCellValueByColumnAndRow(0, $rowIndex, "Итого:");
    $this->format(0, $rowIndex, self::GENERAL_FORMAT, true);

    //Сумма продаж
    $sheet->setCellValueByColumnAndRow(1, $rowIndex, $saleSum);
    $this->format(1, $rowIndex, self::PRICE_FORMAT, true);

    //Количество продаж
    $sheet->setCellValueByColumnAndRow(2, $rowIndex, $saleCount);
    $this->format(2, $rowIndex, self::NUMBER_FORMAT, true);

    //Сумма возвратов
    $sheet->setCellValueByColumnAndRow(3, $rowIndex, '-' . abs($returnSum));
    $this->format(3, $rowIndex, self::PRICE_FORMAT, true);

    //Количество возвратов
    $sheet->setCellValueByColumnAndRow(4, $rowIndex, $returnCount);
    $this->format(4, $rowIndex, self::NUMBER_FORMAT, true);

    //Итого
    $sheet->setCellValueByColumnAndRow(5, $rowIndex, $saleSum - abs($returnSum));
    $this->format(5, $rowIndex, self::PRICE_FORMAT, true);
  }

  /**
   * @param Full[] $sales
   */
  private function writeFullFooter(array $sales) {
    $saleCount = 0;
    $saleSum = 0;
    $returnCount = 0;
    $returnSum = 0;

    $rowsCount = 0;
    $positionsCount = 0;

    foreach ($sales as $sale) {
      $rowsCount += $sale->getItems()->count();
      $positionsCount += $sale->getItems()->count();
      if ($sale->getOperationType() == "SALE") {
        $saleCount++;
        $saleSum += $sale->getSum();
      } else {
        $returnCount++;
        $returnSum -= abs($sale->getSum());
      }
    }

    $rowIndex = 2;
    if (!$this->onlyTotals) {
      $rowIndex += $rowsCount;
    }

    $sheet = $this->excel->getActiveSheet();

    $sheet->setCellValueByColumnAndRow(0, $rowIndex, "Итого:");
    $this->format(0, $rowIndex, self::GENERAL_FORMAT, true);
    $this->format(1, $rowIndex, self::GENERAL_FORMAT, true);

    //Количество чеков
    $sheet->setCellValueByColumnAndRow(2, $rowIndex, $saleCount);
    $this->format(2, $rowIndex, self::NUMBER_FORMAT, true);
    $this->format(3, $rowIndex, self::GENERAL_FORMAT, true);
    $this->format(4, $rowIndex, self::GENERAL_FORMAT, true);

    //Сумма чеков
    $sheet->setCellValueByColumnAndRow(5, $rowIndex, $saleSum - abs($returnSum));
    $this->format(5, $rowIndex, self::PRICE_FORMAT, true);
    $this->format(6, $rowIndex, self::GENERAL_FORMAT, true);
    $this->format(7, $rowIndex, self::GENERAL_FORMAT, true);
    $this->format(8, $rowIndex, self::GENERAL_FORMAT, true);

    //Цена
    $sheet->setCellValueByColumnAndRow(9, $rowIndex, $saleSum);
    $this->format(9, $rowIndex, self::PRICE_FORMAT, true);

    //Количество
    $sheet->setCellValueByColumnAndRow(10, $rowIndex, $positionsCount - abs($returnCount));
    $this->format(10, $rowIndex, self::NUMBER_FORMAT, true);

    //Итого
    $sheet->setCellValueByColumnAndRow(11, $rowIndex, $saleSum - abs($returnSum));
    $this->format(11, $rowIndex, self::PRICE_FORMAT, true);
  }

  /**
   * @return string
   */
  private function write() {
    if ($this->writerType == 'PRINT_HTML') {
      $objWriter = new PHPExcel_Writer_Print_HTML($this->excel, $this->printHtml);
      //Оборачиваем всё в border и центрируем по горизонтали и верикали
      $sheet = $this->excel->getActiveSheet();

      $highestColumn = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn()) - 1);
      $highestRow = $sheet->getHighestDataRow();

      $coordinates = 'A1:' . $highestColumn . $highestRow;

      $sheet->getStyle($coordinates)->applyFromArray([
        'borders'   => [
          'allborders' => [
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => [
              'argb' => 'FFD3D3D3',
            ],
          ],
        ],
        'alignment' => [
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
      ]);
    } else {
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->writerType);
    }
    ob_start();
    $objWriter->save('php://output');

    return ob_get_clean();
  }

  /**
   * @param Full[] $sales
   * @param string $startDate
   * @param string $endDate
   * @param string $format
   * @param string $type
   * @param bool $htmlPrint
   * @param bool $onlyTotals
   * @return bool|string
   */
  public static function generate(array $sales, string $startDate, string $endDate, string $format, string $type, bool $htmlPrint = true, bool $onlyTotals = false) {
    $isShort = $type == Type::SHORT;

    $helper = new ExcelWriter($format, $isShort, $onlyTotals, $htmlPrint);
    $helper->writeSheetName(
      $startDate, $endDate,
      $isShort ? 'Короткий чек' : 'Полный чек'
    );
    $helper->writeHeader();
    $helper->writeSales($sales);
    $helper->writeFooter($sales);

    return $helper->write();
  }
}

class ExcelFileToEmptyException extends Exception {
}
