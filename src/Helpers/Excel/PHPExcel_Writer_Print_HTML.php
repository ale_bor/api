<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers\Excel;

use PHPExcel;
use PHPExcel_Writer_HTML;
use PHPExcel_Writer_IWriter;

class PHPExcel_Writer_Print_HTML extends PHPExcel_Writer_HTML implements PHPExcel_Writer_IWriter {
  /**
   * @var bool
   */
  private $printHtml;

  /**
   * PHPExcel_Writer_Print_HTML constructor.
   * @param PHPExcel $phpExcel
   * @param bool $printHtml
   */
  public function __construct(PHPExcel $phpExcel, bool $printHtml = true) {
    $this->printHtml = $printHtml;
    parent::__construct($phpExcel);
  }

  public function buildCSS($generateSurroundingHTML = true) {
    $css = parent::buildCSS($generateSurroundingHTML);
    $css['table']['width'] = '100%';

    return $css;
  }

  public function generateHTMLFooter() {
    $html = '';
    if ($this->printHtml) {
      $html .= '    <script>window.print();</script>';
      $html .= '    <script>setTimeout(window.close, 0);</script>';
    }

    return $html . parent::generateHTMLFooter();
  }

}
