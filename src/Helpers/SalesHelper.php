<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers;

use App\Entities\Point;
use App\Entities\Sale\Check\Full;
use App\Entities\Sale\Check\FullSaleItem;
use App\Entities\Sale\Check\Type;
use App\Entities\User;
use App\Providers\SalesServiceProvider;
use DateTime;

class SalesHelper {
  public static function rowsEquals(Full $sale, RowContainer $row): bool {
    return $sale->getDate()->getTimestamp() == $row->getDate()->getTimestamp()
      && $sale->getNumber() == $row->getNumber()
      && $sale->getTill() == $row->getTill()
      && $sale->getOperationType() == $row->getType();
  }

  /**
   * Конвертирует массив контейнеров со строками в массив чеков
   * @param RowContainer[] $rows
   * @return Full[]
   */
  public static function rowsToSales(array $rows): array {
    $sales = [];

    /**
     * @var $lastSale Full
     */
    $lastSale = null;
    foreach ($rows as $row) {
      $saleItem = $row->getSaleItem();

      if ($row->isShort()) {
        $sale = self::addSaleItem($row->getSale(), $saleItem);
        $sale->setType(Type::FULL);
        $sales[] = $sale;
      } else {
        if (empty($lastSale) || !self::rowsEquals($lastSale, $row)) {
          $lastSale = $sales[] = $row->getSale();
        }

        if (!empty($lastSale)) {
          self::addSaleItem($lastSale, $saleItem);
        }
      }
    }

    return $sales;
  }

  /**
   * Проверяет массив контейнеров на валидность
   * @param $rows RowContainer[]
   * @return bool
   */
  public static function validateRows(array $rows): bool {
    foreach ($rows as $row) {
      if (!$row->isValid()) {
        return false;
      }
    }

    return true;
  }

  /**
   * Добавляет позицию к чеку
   * @param Full $sale
   * @param FullSaleItem $saleItem
   * @return Full
   */
  public static function addSaleItem(Full $sale, FullSaleItem $saleItem): Full {
    $saleItem->setSale($sale);

    $sale->getItems()->add($saleItem);

    return $sale;
  }

  /**
   * Сравнение чеков на идентичность
   * @param Full $s1
   * @param Full $s2
   * @return bool
   */
  public static function salesEquals(Full $s1, Full $s2): bool {
    return $s1->getDate()->getTimestamp() == $s2->getDate()->getTimestamp()
      && $s1->getNumber() == $s2->getNumber()
      && $s1->getTill() == $s2->getTill()
      && $s1->getOperationType() == $s2->getOperationType();
  }

  /**
   * @param SalesServiceProvider $salesServiceProvider
   * @param Full $sale
   * @param DateTime $dateTime
   * @param Point $point
   * @param User $user
   * @param string[] $dates
   * @param array $errorDates
   */
  public static function processSale(SalesServiceProvider $salesServiceProvider, Full $sale, DateTime $dateTime, Point $point, User $user, array &$dates, array &$errorDates) {
    $full = $salesServiceProvider->getByDate($sale->getType(), $dateTime, $point);
    $exist = false;
    foreach ($full as $item) {
      if (self::salesEquals($sale, $item)) {
        $exist = true;
      }
    }

    if (!$exist) {
      $sale->setUser($user);
      $sale->setPoint($point);
      $salesServiceProvider->create($sale);

      self::uniqueDates($dateTime, $dates);
    } elseif ($exist) {
      $date = DateHelper::formatWithDate($dateTime);
      $errorText = "Данные были загружены ранее, день обновлен частично.";
      foreach ($errorDates as $errorDate) {
        if ($errorDate['date'] == $date && $errorDate['error'] == $errorText) {
          return;
        }
      }
      $errorDates[] = ['date' => $date, 'error' => $errorText];
    }
  }

  /**
   * @param DateTime $dateTime
   * @param string[] $dates
   */
  public static function uniqueDates(DateTime $dateTime, array &$dates) {
    $date = DateHelper::formatWithDate($dateTime);

    if (!in_array($date, $dates)) {
      $dates[] = $date;
    }
  }

  /**
   * @param RowContainer $row
   * @param DateTime $date
   * @return bool
   */
  public static function validateByDate(RowContainer $row, DateTime $date): bool {
    return $row->getDate()->getTimestamp() <= $date->getTimestamp();
  }

  /**
   * @param array $rows
   * @param DateTime $date
   * @return bool
   */
  public static function validateRowsByDate(array $rows, DateTime $date): bool {
    foreach ($rows as $row) {
      if (!self::validateByDate($row, $date)) {
        return false;
      }
    }

    return true;
  }

  /**
   * @param RowContainer[] $rows
   * @param DateTime $date
   * @return RowContainer[]
   */
  public static function filterRowsByDate(array $rows, DateTime $date): array {
    $result = [];

    foreach ($rows as $row) {
      if (self::validateByDate($row, $date)) {
        $result[] = $row;
      }
    }

    return $result;
  }
}
