<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace App\Helpers;

use DateTime;
use DateTimeZone;

class DateHelper {

  /**
   * @param DateTime|null $date
   * @return DateTime
   */
  public static function dateWithoutTime(DateTime $date = null): DateTime {
    if (empty($date)) {
      $date = new DateTime();
    }
    $result = new DateTime(null, $date->getTimezone());
    $result->setTimestamp($date->getTimestamp());
    $result->setTime(0, 0, 0);

    return $result;
  }

  /**
   * @param DateTime|null $date
   * @return DateTime
   */
  public static function timeWithoutDate(DateTime $date = null): DateTime {
    if (empty($date)) {
      $date = new DateTime();
    }
    $result = new DateTime();
    $result->setTimestamp($date->getTimestamp());
    $result->setDate(1970, 1, 1);

    return $result;
  }

  /**
   * @param DateTime $date
   * @param DateTimeZone|null $tz
   * @return DateTime
   * @deprecated
   */
  public static function toUTC(DateTime $date, DateTimeZone $tz = null): DateTime {
    if (empty($tz)) {
      $tz = $date->getTimezone();
    }
    $offset = $tz->getOffset($date);

    $UTCTimezone = new DateTimeZone('UTC');

    $dateTime = new DateTime(null, $UTCTimezone);
    $dateTime->setTimestamp($date->getTimestamp() - $offset);

    return $dateTime;
  }

  /**
   * @param DateTime $date
   * @param DateTimeZone|null $tz
   * @return DateTime
   * @deprecated
   */
  public static function fromUTC(DateTime $date, DateTimeZone $tz = null): DateTime {
    if (empty($tz)) {
      $tz = $date->getTimezone();
    }
    $offset = $tz->getOffset($date);

    $dateTime = new DateTime();
    $dateTime->setTimestamp($date->getTimestamp() + $offset);

    return $dateTime;
  }

  /**
   * @param int $timestamp
   * @param DateTimeZone|null $tz
   * @return DateTime
   * @deprecated
   */
  public static function fromUTCTimestamp(int $timestamp, DateTimeZone $tz = null): DateTime {
    return self::fromUTC(self::dateFromTS($timestamp, new DateTimeZone('UTC')), $tz);
  }

  /**
   * @param int $timestamp
   * @param DateTimeZone|null $tz
   * @return DateTime
   * @deprecated
   */
  public static function toUTCTimestamp(int $timestamp, DateTimeZone $tz = null): DateTime {
    return self::toUTC(self::dateFromTS($timestamp), $tz);
  }

  /**
   * @param int $timestamp
   * @param DateTimeZone|null $tz
   * @return DateTime
   * @deprecated
   */
  public static function dateFromTS(int $timestamp, DateTimeZone $tz = null): DateTime {
    if (empty($tz)) {
      $tz = new DateTimeZone(date_default_timezone_get());
    }
    return date_timestamp_set(new DateTime(null, $tz), $timestamp);
  }

  public static function formatWithDateTime(DateTime $date = null): string {
    if (empty($date)) {
      $date = new DateTime();
    }

    return $date->format('d-m-Y H:i:s');
  }

  public static function formatWithDate(DateTime $date = null): string {
    if (empty($date)) {
      $date = new DateTime();
    }

    return $date->format('d-m-Y');
  }

  public static function formatWithTime(DateTime $date = null): string {
    if (empty($date)) {
      $date = new DateTime();
    }

    return $date->format('H:i:s');
  }

  public static function datePlusTime(DateTime $date, DateTime $time): DateTime {
    $date = self::dateWithoutTime($date);
    $time = self::timeWithoutDate($time);

    return new DateTime(self::formatWithDate($date) . ' ' . self::formatWithTime($time));
  }

}
