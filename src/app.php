<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Providers\GroupsServiceProvider;
use App\Providers\LogServiceProvider;
use App\Providers\PointsServiceProvider;
use App\Providers\SalesServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;

/**
 * @var $app Application
 */

$basePath = $app['basePath'];

$app->register(new PointsServiceProvider());
$app->register(new UsersServiceProvider());
$app->register(new GroupsServiceProvider());
$app->register(new SalesServiceProvider());
$app->register(new LogServiceProvider());

$app->match("/", function (Application $app) {
  return $app->redirect($app['basePath']);
})
  ->method("GET|POST|PUT|DELETE|PATCH");

$app->get("$basePath", function (Application $app) {
  return $app->json([
    'basePath'   => $app['basePath'],
    'apiVersion' => $app['apiVersion'],
  ]);
});

//TODO: В контроллеры
require_once __DIR__ . '/endPoints/users.php';
require_once __DIR__ . '/endPoints/auth.php';
require_once __DIR__ . '/endPoints/groups.php';
require_once __DIR__ . '/endPoints/points.php';
require_once __DIR__ . '/endPoints/sales.php';
require_once __DIR__ . '/endPoints/excel.php';
require_once __DIR__ . '/endPoints/days.php';
require_once __DIR__ . '/endPoints/logs.php';
