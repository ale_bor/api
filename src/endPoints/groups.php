<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Group;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\User;
use App\Providers\GroupsServiceProvider;
use App\Providers\UsersServiceProvider;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->get("$basePath/groups", function (Request $request) use ($app) {
  /**
   * @var $usersProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $currentUser User
   */
  $currentUser = null;
  try {
    if (!$currentUser = $usersServiceProvider->iCanDoThis($request, 'users/me/password')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];

  $groups = [];

  if ($currentUser->isGod() || $currentUser->isSuperAdmin()) {
    $groups = $groupsServiceProvider->getAll();
  } else {
    $groups[] = $currentUser->getGroup();
  }

  return $app->json(DefaultResult::success($groups));
});

$app->put("$basePath/groups", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $name = $request->get('name');

  if ($name != null) {
    $name = trim($name);
  }

  if (empty($name)) {
    return $app->json(DefaultResult::error("Имя не должно быть пустым"));
  }

  $group = new Group();
  $group->setOwner($user);
  $group->setName($name);

  if ($group = $groupsServiceProvider->create($group)) {
    Log::log($app['orm.em'], LogAction::CREATE_GROUP, $user, $group);

    return $app->json(DefaultResult::success($group));
  }

  return $app->json(DefaultResult::error("Не удалось создать группу \"$name\""));
});

$app->get("$basePath/groups/{id}", function (Request $request, $id) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }
  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];

  if ($group = $groupsServiceProvider->getById($id)) {
    return $app->json(DefaultResult::success($group));
  }

  return $app->json(DefaultResult::error("Группа с id $id не найдена"));
});

$app->put("$basePath/groups/{id}", function (Request $request, $id) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }
  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];

  $name = $request->get('name');

  if ($name != null) {
    $name = trim($name);
  }

  if (empty($name)) {
    return $app->json(DefaultResult::error("Имя не должно быть пустым"));
  }

  $group = $groupsServiceProvider->getById($id);
  $group->setName($name);

  if ($group = $groupsServiceProvider->update($group)) {
    Log::log($app['orm.em'], LogAction::CHANGE_GROUP, $user, $group);

    return $app->json(DefaultResult::success($group));
  }

  return $app->json(DefaultResult::error("Не удалось обновить данные группы с id $id"));
});
