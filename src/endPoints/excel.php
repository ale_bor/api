<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\Point;
use App\Entities\PointStatus;
use App\Entities\Sale\Check\Type;
use App\Entities\User;
use App\Helpers\DateHelper;
use App\Helpers\Excel\ExcelWriter;
use App\Helpers\Excel\InvalidExcelFormatException;
use App\Helpers\Excel\Parser\ExcelParserImpl;
use App\Helpers\RowContainer;
use App\Helpers\SalesHelper;
use App\Providers\PointsServiceProvider;
use App\Providers\SalesServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$basePath = $app['basePath'];

//https://regex101.com/r/5zaHSs/11
$fileRegexGroups = /** @lang RegExp */
  '(?(DEFINE)(?<type>(?i)(short|full)(?-i)))' .
  '(?(DEFINE)(?<ext>(?i)(csv|xlsx?|pdf|json|html)(?-i)))' .
  '(?(DEFINE)(?<del>[.,\- \/]))' .
  '(?(DEFINE)(?<day>((3[0-1])|([1-2]\d)|(0?[1-9]))))' .
  '(?(DEFINE)(?<month>(0?[1-9])|(1[0-2])))' .
  '(?(DEFINE)(?<year>\d{2}\d{0,2}))' .
  '(?(DEFINE)(?<date>(?&year)(?&del)(?&month)(?&del)(?&day)))';

$fileRegex = /** @lang RegExp */
  $fileRegexGroups . '(?&type)(?&del)(?&date)(?&del)(?&date)((?&del)totals)?\.(?&ext)';

//download
$app->get("$basePath/download/{pointId}/{file}", function (Application $app, Request $request, int $pointId, array $file) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'download/{pointId}/{file}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  if (!empty($file)) {
    /**
     * @var $salesServiceProvider SalesServiceProvider
     */
    $salesServiceProvider = $app['sales_service_provider'];

    /**
     * @var $pointsServiceProvider PointsServiceProvider
     */
    $pointsServiceProvider = $app['points_service_provider'];

    /**
     * @var $startDate DateTime
     */
    $startDate = $file['start'];
    /**
     * @var $endDate DateTime
     */
    $endDate = $file['end'];

    $startDateFormatted = DateHelper::formatWithDate($startDate);
    $endDateFormatted = DateHelper::formatWithDate($endDate);

    $endDate->add(DateInterval::createFromDateString('1 day'));

    $sales = $salesServiceProvider->getByInterval(Type::FULL, $pointId, $startDate, $endDate);

    $ext = $file['ext'];
    $type = mb_strtoupper($file['type']);
    $onlyTotals = $file['totals'];
    switch ($ext) {
      case 'csv':
      case 'xls':
      case 'xlsx':
      case 'pdf':
        if ($content = ExcelWriter::generate($sales, $startDateFormatted, $endDateFormatted, $ext, $type, false, $onlyTotals)) {
          Log::byPoint($app['orm.em'], LogAction::CREATE_EXCEL, $user, $pointsServiceProvider->getPoint($pointId), [
            'ext'       => $ext,
            'point_id'  => $pointId,
            'type'      => $type,
            'startDate' => $startDateFormatted,
            'endDate'   => $endDateFormatted,
          ]);

          return new Response($content, 200, [
            "Cache-Control"       => "no-cache, must-revalidate",
            "Pragma"              => "no-cache",
            "Content-Disposition" => "attachment; filename=" . $file['name'],
          ]);
        }
        break;
      case 'html':
        if ($content = ExcelWriter::generate($sales, $startDateFormatted, $endDateFormatted, $ext, $type, !$app['debug'], $onlyTotals)) {
          Log::byPoint($app['orm.em'], LogAction::CREATE_EXCEL, $user, $pointsServiceProvider->getPoint($pointId), [
            'ext'       => $ext,
            'point_id'  => $pointId,
            'type'      => $type,
            'startDate' => $startDateFormatted,
            'endDate'   => $endDateFormatted,
          ]);

          return new Response($content, 200, [
            "Cache-Control" => "no-cache, must-revalidate",
            "Pragma"        => "no-cache",
            "Content-type"  => "text/html; charset=UTF-8",
          ]);
        }
        break;
      case 'json':
        Log::byPoint($app['orm.em'], LogAction::CREATE_EXCEL, $user, $pointsServiceProvider->getPoint($pointId), [
          'ext'       => $ext,
          'point_id'  => $pointId,
          'type'      => $type,
          'startDate' => $startDateFormatted,
          'endDate'   => $endDateFormatted,
        ]);

        return $app->json($sales);
    }
  }

  return $app->json(DefaultResult::error("Неверный формат"));
})
  ->assert('file', $fileRegex)
  ->convert('file', function ($file) use ($fileRegexGroups) {
    $fileRegex = /** @lang RegExp */
      '/' .
      $fileRegexGroups .
      '(?<fileType>(?&type))(?&del)(?<startDate>(?&date))(?&del)(?<endDate>(?&date))((?&del)(?<totals>totals))?\.(?<extension>(?&ext))' .
      '/';

    $matches = [];
    if (preg_match($fileRegex, $file, $matches)) {
      $del = /** @lang RegExp */
        '[.,\- \/]';

      $dd1 = mb_split($del, $matches['startDate']);
      $dd2 = mb_split($del, $matches['endDate']);

      return [
        'type'   => $matches['fileType'],
        'start'  => new DateTime("$dd1[2]-$dd1[1]-$dd1[0]"),
        'end'    => new DateTime("$dd2[2]-$dd2[1]-$dd2[0]"),
        'ext'    => $matches['extension'],
        'totals' => !empty($matches['totals']),
        'name'   => $file,
      ];
    }

    return [];
  });

//upload
$app->post("$basePath/sales/upload/v2/{pointId}", function (Application $app, Request $request, int $pointId) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/upload/v2/{pointId}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @param int $pointId
   * @return array
   * @throws Exception
   * @throws InvalidExcelFormatException
   */
  function parse(int $pointId) {
    if (isset($_FILES['file'])) {
      $file = $_FILES['file'];
      $fileName = $file['tmp_name'];

      if (!ExcelParserImpl::isSupportedExcelFormat($file)) {
        throw new InvalidExcelFormatException("Поддерживаются только фарматы XLS, XLSX и CVS");
      }

      $session = sha1(date_timestamp_get(new DateTime()));

      $parser = new ExcelParserImpl($fileName, '', $session);
      $parser->setPointId($pointId);

      if ($parser->getRowsCount() < ExcelParserImpl::HEADER_OFFSET + 1) {
        throw new Exception("Файл пуст");
      }

      $parser->format();

      if (!$parser->isValid()) {
        $parser->purge();
        $message = "";
        foreach ($parser->errors() as $exception) {
          $message .= $exception->getMessage() . "\n";
        }

        throw new Exception($message);
      }

      $parser->copyFile();
      $parser->writeMeta();

      return [
        'turnSize' => $parser->getTurnSize(),
        'hash'     => $parser->hash(),
        'session'  => $session,
      ];
    }

    throw new Exception("Не удалось обработать файл");
  }

  try {
    $parse = parse($pointId);

    Log::log($app['orm.em'], LogAction::PARSE_EXCEL_STARTED, $user, ['success' => true]);

    return $app->json(DefaultResult::success($parse));
  } catch (Exception $e) {
    $message = $e->getMessage();
    Log::log($app['orm.em'], LogAction::PARSE_EXCEL_FAILURE, $user, ['error' => $message]);

    return $app->json(DefaultResult::error($message));
  }
});

$app->patch("$basePath/sales/upload/v2/{session}/{sha1}", function (Application $app, Request $request, string $session, string $sha1) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/upload/v2/{session}/{sha1}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  function parse(array $parsed, SalesServiceProvider $salesServiceProvider, Point $point, User $user) {
    $currentDatePlusDay = DateHelper::dateWithoutTime();
    $interval = DateInterval::createFromDateString('1 day');
    $currentDatePlusDay->add($interval);

    if ($parsed) {
      /**
       * @var $rows RowContainer[]
       */
      $rows = [];
      foreach ($parsed as $row) {
        $rows[] = RowContainer::fromObject($row);
      }

      if (!SalesHelper::validateRows($rows)) {
        throw new Exception("Введены неверные данные");
      }

      /**
       * @var $dates string[]
       */
      $dates = [];

      $filteredRows = SalesHelper::filterRowsByDate($rows, DateHelper::dateWithoutTime($currentDatePlusDay));

      $errorDates = [];

      foreach ($rows as $row) {
        if (array_search($row, $filteredRows) !== false) {
          continue;
        }
        /**
         * @var $row RowContainer
         */
        $dateTime = DateHelper::formatWithDate($row->getDate());
        $errorText = "День ещё не наступил";
        foreach ($errorDates as $errorDate) {
          if ($errorDate['date'] == $dateTime && $errorDate['error'] == $errorText) {
            continue 2;
          }
        }
        $errorDates[] = ['date' => $dateTime, 'error' => $errorText];
      }

      $sales = SalesHelper::rowsToSales($filteredRows);

      foreach ($sales as $sale) {
        $dateTime = $sale->getDate();

        if (!empty($day = $salesServiceProvider->day($point, $dateTime))) {
          if (!$day->isClosed()) {
            SalesHelper::processSale($salesServiceProvider, $sale, $dateTime, $point, $user, $dates, $errorDates);
          } else {
            $dateTime = DateHelper::formatWithDate($sale->getDate());
            $errorText = "День закрыт для редактирования";
            foreach ($errorDates as $errorDate) {
              if ($errorDate['date'] == $dateTime && $errorDate['error'] == $errorText) {
                continue 2;
              }
            }
            $errorDates[] = ['date' => $dateTime, 'error' => $errorText];
          }
        } else {
          SalesHelper::processSale($salesServiceProvider, $sale, $dateTime, $point, $user, $dates, $errorDates);
        }
      }

      return [
        'dates'      => array_values(array_unique($dates)),
        'errorDates' => $errorDates,
      ];
    }

    throw new Exception("Файл пустой");
  }

  $parser = ExcelParserImpl::continue($sha1, $session);
  if ($parser->getPosition() >= $parser->getTurnSize()) {
    $parser->purge();
    $point = $pointsServiceProvider->getPoint($parser->getPointId());

    if ($point->canBeDelete()) {
      $point->setStatus(PointStatus::ACTIVE);
      $point = $pointsServiceProvider->update($point);
    }

    $parse = parse($parser->getData(), $salesServiceProvider, $point, $user);

    return $app->json(DefaultResult::success($parse));
  }
  $parser->runNext();

  return $app->json(DefaultResult::success(['turnPosition' => $parser->getPosition()]));
});

$app->delete("$basePath/sales/upload/v2/{session}/{sha1}", function (Application $app, Request $request, string $session, string $sha1) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/upload/v2/{session}/{sha1}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $parser = ExcelParserImpl::continue($sha1, $session);
  $parser->purge();

  Log::log($app['orm.em'], LogAction::PARSE_EXCEL_CANCELED, $user);

  return $app->json(DefaultResult::empty($sha1));
});
