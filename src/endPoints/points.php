<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\Point;
use App\Entities\User;
use App\Providers\PointsServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->get("$basePath/points", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];
  if ($user->isUser()) {
    if ($user->getPoints()->isEmpty()) {
      return $app->json(DefaultResult::error("Не удалось найти точки пользователя"));
    }
  } else {
    return $app->json(DefaultResult::success($pointsServiceProvider->getAll()));
  }

  return $app->json(DefaultResult::error("Точки не найдены"));
});

$app->put("$basePath/points", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  $name = $request->get('name');
  $roomNumber = $request->get('roomNumber');
  $identifier = $request->get('identifier');
  $status = $request->get('status');
  $checkType = $request->get('checkType');
  $currency = $request->get('currency');

  if (empty($name) || empty($roomNumber) || empty($identifier) || empty($status) || empty($currency) || empty($checkType)) {
    return $app->json(DefaultResult::error("Переданы не все параметры"));
  }

  $point = new Point();
  $point->setName($name);
  $point->setOwner($user);
  $point->setRoomNumber($roomNumber);
  $point->setIdentifier($identifier);
  $point->setStatus($status);
  $point->setCheckType($checkType);
  $point->setCurrency($currency);

  if ($point = $pointsServiceProvider->create($point)) {
    Log::log($app['orm.em'], LogAction::CREATE_POINT, $user, $point);

    return $app->json(DefaultResult::success($point));
  }

  return $app->json(DefaultResult::error("Не удалось создать точку \"$name\""));
});

$app->get("$basePath/points/{id}", function (Request $request, int $id) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];
  if ($point = $pointsServiceProvider->getPoint($id)) {
    return $app->json(DefaultResult::success($point));
  }

  return $app->json(DefaultResult::error("Точка с id $id не найдена"));
});

$app->put("$basePath/points/{id}", function (Request $request, int $id) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $name = $request->get('name');
  $roomNumber = $request->get('roomNumber');
  $identifier = $request->get('identifier');
  $status = $request->get('status');
  $checkType = $request->get('checkType');
  $currency = $request->get('currency');

  $point = $pointsServiceProvider->getPoint($id);

  if (empty($name) && empty($roomNumber) && empty($identifier) && empty($status) && empty($currency) && empty($checkType)) {
    return $app->json(DefaultResult::error("Переданы не все параметры"));
  }

  if (!empty($name)) {
    $point->setName($name);
  }
  if (!empty($roomNumber)) {
    $point->setRoomNumber($roomNumber);
  }
  if (!empty($identifier)) {
    $point->setIdentifier($identifier);
  }
  if (!empty($status)) {
    $point->setStatus($status);
  }
  if (!empty($checkType)) {
    $point->setCheckType($checkType);
  }
  if (!empty($currency)) {
    $point->setCurrency($currency);
  }

  if ($point = $pointsServiceProvider->update($point)) {
    Log::byPoint($app['orm.em'], LogAction::CHANGE_POINT, $user, $point, $point);

    return $app->json(DefaultResult::success($point));
  }

  return $app->json(DefaultResult::error("Не удалось создать точку \"$name\""));
});

$app->get("$basePath/points/{id}/delete", function (int $id, Application $app, Request $request) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  $point = $pointsServiceProvider->getPoint($id);
  if (empty($point)) {
    return $app->json(DefaultResult::error("Точка с id $id не найдена"));
  }

  $users = $usersServiceProvider->getByPoint($point);

  if (!empty($users)) {
    return $app->json(DefaultResult::error("Перед удалением необходимо отвязать пользователей"));
  }

  $pointsServiceProvider->delete($point);

  return $app->json(DefaultResult::empty());
});
