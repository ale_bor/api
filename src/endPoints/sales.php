<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\PointStatus;
use App\Entities\Sale\Check\Full;
use App\Entities\Sale\Check\FullSaleItem;
use App\Entities\Sale\Check\Type;
use App\Entities\Sale\Check\ZReport;
use App\Entities\Sale\Day;
use App\Entities\Sale\DayStatus;
use App\Entities\User;
use App\Providers\PointsServiceProvider;
use App\Providers\SalesServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->get("$basePath/sales/{pointId}/{type}/{startDate}/{endDate}", function (Application $app, Request $request, int $pointId, string $type, string $startDate, string $endDate) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/{pointId}/{type}/{startDate}/{endDate}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  $startDate = new DateTime($startDate);
  $endDate = new DateTime($endDate);

  $sales = $salesServiceProvider->getByInterval($type, $pointId, $startDate, $endDate);

  return $app->json(DefaultResult::success($sales));
});

$app->put("$basePath/sales/{type}", function (Application $app, Request $request, string $type) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/{type}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $checkPointId = $request->get('pointId');
  $checkDateString = $request->get('date');

  if (!validate([$checkPointId, $checkDateString])) {
    return $app->json(DefaultResult::error("Переданы не все поля"));
  }

  $checkPoint = $pointsServiceProvider->getPoint(intval($request->get('pointId')));

  if (empty($checkPoint)) {
    return $app->json(DefaultResult::error("Заведения с id: $checkPointId не найдено"));
  }

  if (!writeAccess($user, $checkPoint->getId())) {
    return $app->json(DefaultResult::error("Запись запрещена"));
  }

  if ($checkPoint->canBeDelete()) {
    $checkPoint->setStatus(PointStatus::ACTIVE);
    $checkPoint = $pointsServiceProvider->update($checkPoint);
  }

  $checkDate = new DateTime($checkDateString);

  $day = $salesServiceProvider->day($checkPoint, $checkDate);
  if (!empty($day) && $day->isClosed() && $user->isUser()) {
    return $app->json(DefaultResult::error("День закрыт. Для изменения данных обратитесь к администратору."));
  }

  $newCheck = null;
  $checkOperationType = $request->get('operationType');

  switch ($type) {
    case Type::ZREPORT:
      $checkCount = $request->get('count');
      $checkSum = $request->get('sum');

      if (!validate([$checkCount, $checkSum])) {
        return $app->json(DefaultResult::notReceived([
          'count' => !empty($checkCount),
          'sum'   => !empty($checkSum),
        ]));
      }

      $newCheck = new ZReport();
      $newCheck->setDate($checkDate);
      $newCheck->setCount($checkCount);
      $newCheck->setSum($checkSum);
      $newCheck->setStatus("SALE");
      $newCheck->setUser($user);
      $newCheck->setPoint($checkPoint);
      break;
    case Type::FULL:
      $checkNumber = $request->get('number');
      $checkTill = $request->get('till');
      $checkItems = $request->get('items');

      if (!validate([$checkOperationType, $checkNumber, $checkTill, $checkItems])) {
        return $app->json(DefaultResult::notReceived([
          'operationType' => !empty($checkOperationType),
          'number'        => !empty($checkNumber),
          'till'          => !empty($checkTill),
          'items'         => !empty($checkItems),
        ]));
      }

      $newCheck = new Full();
      $newCheck->setDate($checkDate);
      $newCheck->setNumber($checkNumber);
      $newCheck->setTill($checkTill);
      $newCheck->setOperationType($checkOperationType);
      $newCheck->setUser($user);
      $newCheck->setPoint($checkPoint);

      $newCheck = $salesServiceProvider->create($newCheck);

      foreach ($checkItems as $checkItem) {
        $fullSaleItem = new FullSaleItem();
        $fullSaleItem->setSale($newCheck);
        $fullSaleItem->setArticle($checkItem['article']);
        $fullSaleItem->setName($checkItem['name']);
        $fullSaleItem->setPrice(abs($checkItem['price']));
        $fullSaleItem->setAmount(abs($checkItem['amount']));
        $discount = $checkItem['discount'];
        $discount = $discount == 'null' || $discount == 'undefined' ? 0 : abs($discount);
        $fullSaleItem->setDiscount($discount);
        $newCheck->getItems()->add($fullSaleItem);
      }
      break;
    default:
      return $app->json(DefaultResult::error("Не указан тип чека"));
  }

  if ($newCheck = $salesServiceProvider->create($newCheck)) {
    Log::byPoint($app['orm.em'], LogAction::CREATE_SALE, $user, $newCheck->getPoint(), $newCheck);

    if (empty($day)) {
      $day = new Day();
      $day->setPoint($checkPoint);
      $day->setDate($checkDate);
      $salesServiceProvider->createDay($day);
    }
    $day->setStatus(DayStatus::SALE);
    $salesServiceProvider->updateDay($day);

    return $app->json(DefaultResult::success($newCheck));
  }

  return $app->json(DefaultResult::error("Не удалось создать чек"));
});

$app->delete("$basePath/sales/{id}", function (Application $app, Request $request, int $id) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'sales/delete/{id}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  $sale = $salesServiceProvider->getById(null, $id);

  if (!writeAccess($user, $sale->getPoint()->getId())) {
    return $app->json(DefaultResult::error("Запись запрещена"));
  }

  $day = $salesServiceProvider->day($sale->getPoint(), $sale->getDate());
  if (!empty($day) && $day->isClosed() && $user->isUser()) {
    return $app->json(DefaultResult::error("День закрыт. Для изменения данных обратитесь к администратору."));
  }

  $salesServiceProvider->delete($id);

  return $app->json(DefaultResult::empty());
});

/**
 * @param array $values
 * @return bool
 */
function validate(array $values): bool {
  foreach ($values as $value) {
    if ($value == null) {
      return false;
    }
  }

  return true;
}

function readAccess(User $user, int $pointId): bool {
  foreach ($user->getPoints() as $point) {
    if ($point->getPoint()->getId() == $pointId) {
      return true;
    }
  }

  return false;
}

/**
 * Проверяет права на запись у юзера
 * @param User $user
 * @param int $pointId
 * @return bool
 */
function writeAccess(User $user, int $pointId): bool {
  foreach ($user->getPoints() as $point) {
    if ($point->getPoint()->getId() == $pointId) {
      return $point->isWrite();
    }
  }

  return false;
}
