<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Sale\Check\Type;
use App\Entities\Sale\Day;
use App\Entities\Sale\DayStatus;
use App\Entities\User;
use App\Entities\UserPoint;
use App\Helpers\DateHelper;
use App\Helpers\DateParser;
use App\Helpers\ValidationHelper;
use App\Providers\PointsServiceProvider;
use App\Providers\SalesServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

//digest
$app->get("$basePath/days/digest/{startDate}/{endDate}", function (Application $app, Request $request, DateTime $startDate, DateTime $endDate) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'days/digest/{startDate}/{endDate}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $digests = [];

  $interval = DateInterval::createFromDateString('1 day');
  $endDate->add($interval);

  /**
   * @var $point UserPoint
   */
  foreach ($user->getPoints()->toArray() as $point) {
    $period = new DatePeriod($startDate, $interval, $endDate);

    $pointDigests = [];

    /**
     * @var $date DateTime
     */
    foreach ($period as $date) {
      $dayStatus = DayStatus::NODATA;
      $dayClosed = false;
      $sum = 0;

      if (!empty($day = $salesServiceProvider->day($point->getPoint(), $date))) {
        $dayStatus = $day->getStatus();
        $dayClosed = $day->isClosed();
        $type = $point->getPoint()->getCheckType() == Type::ZREPORT ? Type::ZREPORT : Type::FULL;
        $pointId = $point->getPoint()->getId();
        $sum = $salesServiceProvider->getSalesSum($type, $date, $pointId);
      }

      $pointDigests[] = [
        'date'   => DateHelper::formatWithDate($date),
        'status' => $dayStatus,
        'sum'    => $sum,
        'closed' => $dayClosed,
      ];
    }

    $digests[] = [
      'point'   => $point,
      'digests' => $pointDigests,
    ];
  }

  return $app->json(DefaultResult::success($digests));
})
  ->assert('startDate', ValidationHelper::DATE_YYYY_MM_DD)
  ->assert('endDate', ValidationHelper::DATE_YYYY_MM_DD)
  ->convert('startDate', new DateParser())
  ->convert('endDate', new DateParser());

$app->get("$basePath/days/{pointId}/{date}/status", function (Application $app, Request $request, int $pointId, DateTime $date) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'days/{pointId}/{date}/status')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $dayStatus = DayStatus::NODATA;
  $point = $pointsServiceProvider->getPoint($pointId);

  $day = $day = $salesServiceProvider->day($point, $date);

  if (empty($day)) {
    $day = new Day();
    $day->setDate($date);
    $day->setPoint($point);
    $day->setStatus($dayStatus);
    $day->setClosed(false);

    $day = $salesServiceProvider->createDay($day);
  }

  return $app->json(DefaultResult::success($day));
})
  ->assert('date', ValidationHelper::DATE_YYYY_MM_DD)
  ->convert('date', new DateParser());

$app->put("$basePath/days/status", function (Application $app, Request $request) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'days/status')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $dayModel = $request->get('day');

  $date = new DateTime($dayModel['date']);
  $point = $pointsServiceProvider->getPoint($dayModel['pointId']);

  $day = $salesServiceProvider->day($point, $date);
  if (empty($day)) {
    $day = new Day();
    $day->setPoint($point);
    $day->setDate($date);
    $salesServiceProvider->createDay($day);
  }
  $day->setStatus($dayModel['status']);
  $day->setClosed($dayModel['closed'] === 'true');
  $salesServiceProvider->updateDay($day);

  return $app->json(DefaultResult::success($day));
});

$app->delete("$basePath/days/{pointId}/{date}", function (Application $app, Request $request, int $pointId, DateTime $date) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'days/{pointId}/{date}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  /**
   * @var $salesServiceProvider SalesServiceProvider
   */
  $salesServiceProvider = $app['sales_service_provider'];

  $startDate = DateHelper::dateWithoutTime($date);
  $endDate = DateHelper::dateWithoutTime($date);
  $interval = DateInterval::createFromDateString('1 day');
  $endDate->add($interval);

  foreach ($user->getPoints() as $userPoint) {
    $point = $userPoint->getPoint();
    if ($point->getId() == $pointId) {
      $day = $salesServiceProvider->day($point, $startDate);

      if (!empty($day) && $day->isClosed()) {
        return $app->json(DefaultResult::error("День закрыт"));
      }

      if (empty($day) || !$day->isClosed()) {
        $salesServiceProvider->deleteByDate($point->getCheckType(), $pointId, $startDate, $endDate);

        if (!empty($day)) {
          $day->setStatus(DayStatus::NODATA);
          $salesServiceProvider->updateDay($day);
        }
      }
    }
  }

  return $app->json(DefaultResult::empty());
})
  ->assert('date', ValidationHelper::DATE_YYYY_MM_DD)
  ->convert('date', new DateParser());
