<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\User;
use App\Helpers\DateParser;
use App\Helpers\ValidationHelper;
use App\Providers\LogServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->get("$basePath/logs/{startDate}/{endDate}", function (Application $app, Request $request, DateTime $startDate, DateTime $endDate) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $logsServiceProvider LogServiceProvider
   */
  $logsServiceProvider = $app['logs_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'logs/{startDate}/{endDate}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $logs = $logsServiceProvider->getByInterval($startDate, $endDate);

  return $app->json(DefaultResult::success($logs));
})
  ->assert('startDate', ValidationHelper::DATE_YYYY_MM_DD)
  ->assert('endDate', ValidationHelper::DATE_YYYY_MM_DD)
  ->convert('startDate', new DateParser())
  ->convert('endDate', new DateParser());
