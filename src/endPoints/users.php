<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\DefaultResult;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\Meta;
use App\Entities\PointStatus;
use App\Entities\Role;
use App\Entities\Status;
use App\Entities\User;
use App\Entities\UserPoint;
use App\Providers\GroupsServiceProvider;
use App\Providers\PointsServiceProvider;
use App\Providers\UsersServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->get("$basePath/users", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  switch ($user->getRole()) {
    case Role::GOD:
    case Role::SUPERADMIN:
      if ($users = $usersServiceProvider->getAll()) {
        return $app->json(DefaultResult::success($users));
      }
      break;
    case Role::ADMIN:
      return $app->json(DefaultResult::success($usersServiceProvider->getByGroup($user->getGroup()->getId())));
  }

  return $app->json(DefaultResult::empty());
});

$app->put("$basePath/users", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  /**
   * @var $currentUser User
   */
  $currentUser = null;
  try {
    if (!$currentUser = $usersServiceProvider->iCanDoThis($request, 'users')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $userName = $request->get('name');
  $userEmail = $request->get('email');
  $userPassword = $request->get('password');
  $userRole = $request->get('role');
  $userGroup = $request->get('group');
  $userStatus = $request->get('status');

  if (empty($userName) || empty($userEmail) || empty($userPassword) || empty($userRole) || empty($userStatus)) {
    return $app->json(new DefaultResult(Status::error, Meta::n(), null, "Переданы не все поля"));
  }

  if ($userByEmail = $usersServiceProvider->getByEmail($userEmail)) {
    return $app->json(DefaultResult::error("Пользователь с таким адресом электронной почты уже существует"));
  }

  if (strlen($userPassword) < 6) {
    return $app->json(DefaultResult::error("Длинна пароля должна быть 6 или более символов"));
  }
  if (($userRole != Role::SUPERADMIN || $userRole != Role::GOD) && empty($userGroup)) {
    return $app->json(DefaultResult::error("Не указана группа пользователя"));
  }

  $user = new User();
  $user->setName($userName);
  $user->setEmail($userEmail);
  $user->setRole($userRole);
  $user->setStatus($userStatus);
  $user->setGroup($groupsServiceProvider->getById($userGroup['id']));
  $user->setPassword(UsersServiceProvider::securePassword($userPassword));

  if (!empty($pointsBuf = $request->get('points'))) {
    foreach ($pointsBuf as $item) {
      $point = new UserPoint();
      $point->setUser($user);
      $point1 = $pointsServiceProvider->getPoint(intval($item['point']));

      if ($point1->canBeDelete()) {
        $point1->setStatus(PointStatus::ACTIVE);
        $point1 = $pointsServiceProvider->update($point1);
      }

      $point->setPoint($point1);
      $point->setInputType($item['inputType']);
      $point->setWrite($item['write'] == "true");
      $user->getPoints()->add($point);
    }
  }

  if ($user = $usersServiceProvider->create($user)) {
    Log::log($app['orm.em'], LogAction::CREATE_USER, $currentUser, $user);

    $messageText = <<<HTML
<p>
Ваш аккаунт на сайте <a href='http://rvdp.alebor.redprojects.tk'>RVDP</a> успешно создан.<br/>
Для входа используйте следующие данные:<br/>E-mail: $userEmail<br/>Пароль: $userPassword
</p>
HTML;

    $message = Swift_Message::newInstance()
      ->setSubject('[RVDP] Ваши учётные данные')
      ->setFrom(['admin@redprojects.tk'])
      ->setTo([$userEmail])
      ->setBody($messageText, 'text/html');

    $app['mailer']->send($message);

    return $app->json(DefaultResult::success($user));
  }

  return $app->json(DefaultResult::error("Не удалось создать пользователя"));
});

$app->get("$basePath/users/{id}", function (int $id, Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  $currentUser = null;
  try {
    if (!$currentUser = $usersServiceProvider->iCanDoThis($request, 'users/{id}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  if ($id == 0) {
    return $app->json(DefaultResult::success($currentUser));
  }

  $user = null;
  if ($currentUser->getRole() == Role::USER) {
    if ($id == $currentUser->getId()) {
      $user = $usersServiceProvider->getUser($id);
    } else {
      return $app->json(DefaultResult::noAccess());
    }
  } else {
    $user = $usersServiceProvider->getUser($id);
  }

  if ($user) {
    return $app->json(DefaultResult::success($user));
  } else {
    return $app->json(DefaultResult::error("Пользователь с id $id не найден"));
  }
});

$app->put("$basePath/users/{id}", function (Request $request, $id) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];
  /**
   * @var $groupsServiceProvider GroupsServiceProvider
   */
  $groupsServiceProvider = $app['groups_service_provider'];
  /**
   * @var $pointsServiceProvider PointsServiceProvider
   */
  $pointsServiceProvider = $app['points_service_provider'];

  $currentUser = null;
  try {
    if (!$currentUser = $usersServiceProvider->iCanDoThis($request, 'users/{id}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $userPassword = $request->get('password');
  $userName = $request->get('name');
  $userEmail = $request->get('email');
  $userRole = $request->get('role');
  $userGroup = $request->get('group');
  $userStatus = $request->get('status');

  /**
   * @var $changedUser User
   */
  $changedUser = $usersServiceProvider->getUser($id);

  if (empty($changedUser)) {
    return $app->json(DefaultResult::error("Пользователь с id $id не найден"));
  }

  if (!empty($userPassword) && $userPassword != 'null' && $userPassword != 'undefined') {
    $changedUser->setPassword(UsersServiceProvider::securePassword($userPassword));
  }

  if ($currentUser->isNotUser()) {
    if (!empty($userName)) {
      $changedUser->setName($userName);
    }
    if (!empty($userEmail)) {
      $changedUser->setEmail($userEmail);
    }
    if (!empty($userRole)) {
      //Нельзя менять роль у себя, если ты админ
      if ($currentUser->getId() != $changedUser->getId()) {
        $changedUser->setRole($userRole);
      }
    }
    if (!empty($userGroup)) {
      $changedUser->setGroup($groupsServiceProvider->getById($userGroup['id']));
    }
    if (!empty($userStatus)) {
      //Нельзя менять статус у себя, если ты админ
      if ($currentUser->getId() != $changedUser->getId()) {
        $changedUser->setStatus($userStatus);
      }
    }

    $pointsBuf = $request->get('points');

    if (!empty($pointsBuf) && is_array($pointsBuf)) {
      $changedUser->getPoints()->clear();
      $changedUser = $usersServiceProvider->update($changedUser);
      foreach ($pointsBuf as $item) {
        $point = new UserPoint();
        $point->setUser($changedUser);
        $point1 = $pointsServiceProvider->getPoint(intval($item['point']));

        if ($point1->canBeDelete()) {
          $point1->setStatus(PointStatus::ACTIVE);
          $point1 = $pointsServiceProvider->update($point1);
        }

        $point->setPoint($point1);
        $point->setWrite($item['write'] == "true");
        $point->setInputType($item['inputType']);
        $changedUser->getPoints()->add($point);
      }
    }
  }

  if ($changedUser = $usersServiceProvider->update($changedUser)) {
    Log::log($app['orm.em'], LogAction::CHANGE_USER, $currentUser, $changedUser);

    return $app->json(DefaultResult::success($changedUser));
  }

  return $app->json(DefaultResult::error("Не удалось обновить пользоватлея с id $id"));
});

$app->get("$basePath/users/{id}/delete", function (int $id, Request $request, Application $app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  $currentUser = null;
  try {
    if (!$currentUser = $usersServiceProvider->iCanDoThis($request, 'users/{id}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  if ($currentUser->getId() == $id) {
    return $app->json(DefaultResult::error("Нельзя удалить самого себя"));
  }

  $user = $usersServiceProvider->getUser($id);

  if (empty($user)) {
    return $app->json(DefaultResult::error("Пользователь с id $id не найден"));
  }

  if (!$user->canBeDelete()) {
    return $app->json(DefaultResult::error("Пользователь уже совершал действия и не может быть удалён"));
  }

  $usersServiceProvider->delete($user);

  return $app->json(DefaultResult::empty());
});
