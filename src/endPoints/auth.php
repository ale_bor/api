<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\Entities\BlockedStatus;
use App\Entities\DefaultResult;
use App\Entities\Log;
use App\Entities\LogAction;
use App\Entities\User;
use App\Providers\UsersServiceProvider;
use Symfony\Component\HttpFoundation\Request;

$basePath = $app['basePath'];

$app->match("$basePath/auth", function (Request $request) use ($app) {
  $email = $request->get('email');
  $password = $request->get('password');

  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  if (!empty($email) && !empty($password) && $user = $usersServiceProvider->validateLoginPassword($email, $password)) {
    if ($user->isBlocked()) {
      return $app->json(DefaultResult::error("Пользователь заблокирован"));
    }

    $token = $usersServiceProvider->generateToken($user);

    if ($user->canBeDelete()) {
      $user->setStatus(BlockedStatus::DEFAULT);

      $user = $usersServiceProvider->update($user);
      Log::log($app['orm.em'], LogAction::CHANGE_USER, $user, ['ip' => $request->getClientIp()]);
    }

    Log::log($app['orm.em'], LogAction::AUTH, $user, ['ip' => $request->getClientIp()]);

    return $app->json(DefaultResult::success($user, $token->getToken()));
  }

  return $app->json(DefaultResult::error("Неверная пара логин - пароль", [$email, sha1($password)]));
})->method('get|post');

$app->delete("$basePath/auth", function (Request $request) use ($app) {
  /**
   * @var $usersServiceProvider UsersServiceProvider
   */
  $usersServiceProvider = $app['users_service_provider'];

  /**
   * @var $user User
   */
  $user = null;
  try {
    if (!$user = $usersServiceProvider->iCanDoThis($request, 'auth/{token}')) {
      return $app->json(DefaultResult::noAccess());
    }
  } catch (DefaultResult $exceptionResult) {
    return $app->json($exceptionResult);
  }

  $token = $request->get('token');

  if (!empty($token) && $usersServiceProvider->removeToken($token)) {

    Log::log($app['orm.em'], LogAction::DE_AUTH, $user, ['ip' => $request->getClientIp(), 'token' => $token]);

    return $app->json(DefaultResult::empty());
  }

  return $app->json(DefaultResult::error("Неверный токен", $request->request->all()));
});
