<?php
/**
 * (c) 2017 Onofrey Boris <redcreepster@gmail.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (php_sapi_name() != 'cli-server') {
  die('Only for developers');
}

require_once __DIR__ . "/index.php";
